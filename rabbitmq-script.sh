#!/bin/bash

rabbitmq-plugins enable --offline rabbitmq_mqtt rabbitmq_web_mqtt rabbitmq_federation_management rabbitmq_stomp
rabbitmqctl add_user mqtt-IoT-device mqtt-IoT-device
rabbitmqctl set_user_tags mqtt-IoT-device management
rabbitmqctl set_permissions -p "/" mqtt-IoT-device ".*" ".*" ".*"
rabbitmqctl stop;