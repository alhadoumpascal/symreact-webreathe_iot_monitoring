import { IoTDeviceAdapter } from "../../adapters/IoTDeviceAdapter";
import EnvConfig from "../../config/env.config";
import { TClimateDeviceDataDto, TIoTDeviceDto, TSmartEnergyDeviceDataDto } from "../../dto/iotDevice.dto";
import { I_IoTDeviceType } from "../../types/IoTDeviceType";
import { fetcher, TLdJSONCollection } from "./api";
import * as CRUD from "./crud";
import moment from "moment";

const ROOT_URL = String(EnvConfig.API_URL);
const IOT_DEVICES_URL = ROOT_URL + "iot-devices";

export type TDateRange = {
    startDate: string; 
    endDate: string;
};

export const dataDeviceUrl = {
    CLIMATE: ROOT_URL + "climate-devices/data",
    "SMART ENERGY METER": ROOT_URL + "smart-energy-meter-devices/data",
    LIGHT: ROOT_URL + "light-devices/data",
}

let response: any;
const nowDate = moment(new Date()).format("YYYY-MM-DD");

export async function add(iotDevice: TIoTDeviceDto) {
    console.log("======== On create operation ========");

    try {
        response = await CRUD.create(iotDevice, IOT_DEVICES_URL);
    } catch (error) {
        console.log(error);
        return Promise.reject(error);
    }

    return response;
}

export async function update(iotDevice: TIoTDeviceDto) {
    
    console.log("======== On update operation ========");
    const URL = IOT_DEVICES_URL + "/" + iotDevice.id;

    try {
        response = await CRUD.update(iotDevice, URL);
    } catch (error) {
        console.log(error);
        return Promise.reject(error);
    }

    return response;
}

export async function remove(deviceId: number | string) {
    
    console.log("======== On delete operation ========");
    const URL = IOT_DEVICES_URL + "/" + deviceId;

    try {
        response = await CRUD.remove(URL);
        // return new Promise((resolve, reject) => {
        //     setTimeout(() => {
        //         resolve("Successfully deleted");
        //     }, 5000);
        // })
    } catch (error) {
        console.log(error);
        return Promise.reject(error);
    }

    return response;
}

export async function getAll() {
    let devices: TIoTDeviceDto[] = [];
    try {
        const res = (await fetcher<"GET", TIoTDeviceDto[]>({url: IOT_DEVICES_URL})) as TLdJSONCollection<TIoTDeviceDto[]>; // Fetch the devices data
        // console.log(res["hydra:member"]);
        devices = res["hydra:member"] as TIoTDeviceDto[];
    } catch (error) {
        console.log(error);
        return Promise.reject(error);
    }
    
    return devices.map((device: any) => IoTDeviceAdapter.adapt(device)); // Adapt the backend's devices object type to the frontend's devices object
};

export async function getById(deviceId: string | number) {
    
    let device: any;
    try {
        const res = await fetcher<"GET", TIoTDeviceDto>({url: IOT_DEVICES_URL + `/${deviceId}`}); // Fetch the devices data
        // console.log(res);
        device = res;
    } catch (error) {
        console.log(error);
        return Promise.reject(error);
    }
    
    return IoTDeviceAdapter.adapt(device); // Adapt the backend's device object type to the frontend's device object
};

export async function getData(deviceId: string | number, dateRange: TDateRange) {
    let data: any;
    const url = new URL(IOT_DEVICES_URL + "/" + deviceId + "/data");

    for (const dateParam in dateRange) {
        url.searchParams.delete(dateParam)
        url.searchParams.append(dateParam, dateRange[dateParam as keyof TDateRange]);    
    }
    
    // console.log(url.toString());
  
    try {
        const res = await fetcher<"GET", any>({url: url.toString()}); // Fetch the devices data
        // console.log(res);
        data = res;
    } catch (error) {
        console.log(error);
        return Promise.reject(error);
    }
    
    return data;
}

export async function saveData(data: TClimateDeviceDataDto | TSmartEnergyDeviceDataDto, iotDevice: I_IoTDeviceType) {

    const URL = dataDeviceUrl[iotDevice.deviceType];

    try {
        response = await CRUD.create(data, URL);
    } catch (error) {
        console.log(error);
        return Promise.reject(error);
    }

    return response;
}
