import { CustomError } from "../../utils/error";

type Method = "GET" | "POST" | "PUT" | "DELETE";

export type TMeta = {
  "@id": string;
  "@type": string;
  "@context"?: string;
};

export type TLdJSONCollection<T> = {
  "hydra:member": (T & TMeta)[] | [];
  "hydra:totalItems"?: number;
  "hydra:view"?: {
    "@id": string;
    "@type": string;
    "hydra:first": string;
    "hydra:last": string;
    "hydra:previous": string;
    "hydra:next": string;
  };
  "hydra:search"?: {
    "@type": string;
    "hydra:template": string;
    "hydra:variableRepresentation": string;
    "hydra:mapping": {
      "@type": string;
      variable: string;
      property: string;
      required: boolean;
    }[];
    
  };
}

type ApiResponse<T> = {
  GET: TLdJSONCollection<T> | (T & TMeta);
  POST: (T & TMeta);
  PUT: (T & TMeta);
  DELETE: (T & TMeta) | null;
};

export type TRequestInfo = {
  url: string; 
}

type HttpMethod = {
  GET: "GET";
  POST: "POST";
  PUT: "PUT";
  DELETE: "DELETE";
};

type ResponseType<M extends Method, T> = ApiResponse<T>[M];

export async function fetcher<M extends Method, T>(
  input: TRequestInfo,
  init?: RequestInit & { method: HttpMethod[M] }
) {
  const token = window.localStorage.getItem("authToken") || "";

  let response: Response;
  let headers: HeadersInit = {
    "Content-Type": "application/json",
    // "Accept": "application/json",
    ...init?.headers,
  };

  if(!!token) {
    headers = {
      ...headers,
      Authorization: `Bearer ${token}`,
    }
  }

  response = await fetch(input.url, {
    ...init,
    headers,
  });
  // console.log(response);
  if (response.ok) {
    
    try {
      return (await response.json()) as ResponseType<M, T>;
    } catch (error) {
      console.log(error);
      return Promise.resolve({
        status: response.status,
        message: response.statusText
      })
    }
    
  } else {
    
    try {
      let err = await response.json();
        // console.log(err);
        if (err) {
          err = err.violations ? err.violations : err.message;
          // console.log("1.", clgMessage);
          return Promise.reject({
            status: response.status,
            message: response.status === 400 ? response.statusText : err || response.statusText,
          });
          // throw new Error(err.violations);
          // return err.violations;
        } else {
          console.log('===== Else ======');
        }
      } catch (e: any) {
        console.log("2.", e);
        return Promise.reject({
          status: response.status,
          message: response.statusText,
        });
        // throw new Error(response.statusText);
      }
  }
}
