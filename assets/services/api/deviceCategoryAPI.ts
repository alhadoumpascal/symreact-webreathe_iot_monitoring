import { DeviceCategryAdapter } from "../../adapters/DeviceCategoryAdapter";
import EnvConfig from "../../config/env.config";
import { TDeviceCategoryDto } from "../../dto/deviceCategory.dto";
import { IDeviceCategoryType } from "../../types/DeviceCategoryType";
import { fetcher, TLdJSONCollection } from "./api";
import { CRUD } from "./crud";

const ROOT_URL = String(EnvConfig.API_URL);
const DEVICE_CATEGORY_URL = ROOT_URL + "device-categories";
console.log(DEVICE_CATEGORY_URL);

let response: any;
export async function add(category: IDeviceCategoryType) {
    const device = DeviceCategryAdapter.reverse(category); // Adapt the frontend's iot device category object intto the backend's iot device category object type
    try {
        response = await CRUD.create(device, DEVICE_CATEGORY_URL);
    } catch (error) {
        console.log(error);
        Promise.reject(error);
    }

    return response;
}

export async function getAll() {
    let deviceCategories: TDeviceCategoryDto[] = [];
    try {
        const res = await fetcher<"GET", TDeviceCategoryDto[]>({url: DEVICE_CATEGORY_URL}) as TLdJSONCollection<TDeviceCategoryDto[]>; // Fetch the iot device Categories data
        console.log(res);
        deviceCategories = res["hydra:member"] as TDeviceCategoryDto[];
    } catch (error) {
        console.log(error);
        Promise.reject(error);
    }
    
    return deviceCategories.map((deviceCategory: TDeviceCategoryDto) => DeviceCategryAdapter.adapt(deviceCategory)); // Adapt the backend's iot device Categories array object type to the frontend's iot device Categories array object type 
    
};