import mqtt from "mqtt";
import EnvConfig from "../config/env.config";
import { TMqttWsOptions } from "../types/MqttWsType";

// const websocketUrl = `ws://${EnvConfig.MQTT_WS_HOST}:${
//     EnvConfig.MQTT_WS_PORT}/ws`;
const websocketUrl = `ws://${EnvConfig.BROKER_HOST}:${EnvConfig.MQTT_WS_PORT}/ws`;

const apiEndpoint = "";

// console.log(websocketUrl);

function getClient(
  {clientId, topic,
  setConnectionStatus} : TMqttWsOptions,
  errorHandler: Function
) {
  const client = mqtt.connect(websocketUrl, {
    // connectTimeout: 30000,
    // keepalive: 60,
    clientId:
      clientId || "myclientid_" + parseInt(String(Math.random() * 100), 10),
    username: EnvConfig.MQTT_WS_USERNAME,
    password: EnvConfig.MQTT_WS_PASSWORD,
    reconnectPeriod: 0,
  });

  /*client.on("connect", () => {
    console.log(`${clientId} - CONNECTION SUCCESS`);
    setConnectionStatus(true);
  });

  client.on("offline", () => {
    setConnectionStatus(false);
    console.log(`======= onOffline ========= ${clientId}`);
  });

  client.on("reconnect", () => {
    setConnectionStatus(false);
    console.log(`======= onReconnect ========= ${clientId}`);
  });

  client.on("end", () => {
    setConnectionStatus(false);
    console.log(`======= onEnd ========= ${clientId}`);
  });

  client.on("error", (err: any) => {
    console.log("========== OnError Event ========== ");
    errorHandler(`Connection to ${websocketUrl} failed`);
    console.log("========== End OnError Event ========== ");
    setConnectionStatus(false);
    client.end();
  });*/

  client
      .on("connect", () => {
        console.log(`${clientId} - CONNECTION SUCCESS`);
        setConnectionStatus(true);

        client.subscribe(
            topic,
          { qos: 1 },
          (err: any, granted: any) => {
            console.log(
              `========== OnSubscribe Event ========== ${clientId}`
            );
            if (err) {
              console.log("Subscription request failed");
            } else console.log(granted);
            console.log("========== End OnSubscribe Event ========== ");
          }
        );
      })
      .on("offline", () => {
        setConnectionStatus(false);
        console.log(`======= onOffline ========= ${clientId}`);
      })
      .on("reconnect", () => {
        setConnectionStatus(false);
        console.log(`======= onReconnect ========= ${clientId}`);
      })
      .on("end", () => {
        setConnectionStatus(false);
        console.log(`======= onEnd ========= ${clientId}`);
      })
      .on("error", (err: any) => {
        console.log(
          `========== OnError Event ========== ${clientId}`
        );
        console.log(`Connection to ${websocketUrl} failed`);
        console.log("========== End OnError Event ========== ");
        setConnectionStatus(false);
      })
      .subscribe(topic, { qos: 1 }, (err: any, granted: any) => {
        console.log(
          `========== OnSubscribe Event ========== ${clientId}`
        );
        if (err) {
          console.log("Subscription request failed");
        } else console.log(granted);
        console.log("========== End OnSubscribe Event ========== ");
      })
      .on("close", () => {
        console.log(
          `========== OnClose Event ========== ${clientId}`
        );
        console.log("Connection close");
        console.log("========== End OnClose Event ========== ");
      })
      .on("disconnect", (packet: mqtt.IPacket) => {
        console.log(
          `========== OnDisconnect Event ========== ${clientId}`
        );
        console.log("packet :", packet);
        console.log("========== End OnDisconnect Event ========== ");
      })
    //   .on(
    //     "message",
    //     (topic: string, message: Buffer, packet: mqtt.IPublishPacket) => {
    //       console.log(
    //         `"========== OnMessage Event ========== ${clientId}`
    //       );
    //       console.log("topic :", topic);
    //       console.log("message :", message);
    //       const strMess = new TextDecoder("utf-8").decode(message);
    //       console.log("message as string :", strMess);
    //       // callBack(JSON.parse(new TextDecoder("utf-8").decode(message))); packet: mqtt.IPublishPacket
    //       console.log("========== End OnMessage Event ========== ");
    //     }
    //   );

  return client;
}

function subscribe(
  client: mqtt.MqttClient,
  topic: string,
  errorHandler: Function
) {
  console.log("========== OnSubscribe Event ========== ");
  const callBack = (err: any, granted: any) => {
    if (err) {
      errorHandler("Subscription request failed");
    } else console.log(granted);
  };
  console.log("========== End OnSubscribe Event ========== ");
  //   return client.subscribe(topic, callBack);
  return client.subscribe(apiEndpoint + topic, { qos: 1 }, callBack);
}

function publish(
  client: mqtt.MqttClient,
  topic: string,
  message: string,
  handler: Function
) {
  console.log("========== OnPublish Event ========== ");

  const callBack = (
    error?: Error | undefined,
    packet?: mqtt.Packet | undefined
  ): any => {
    if (error) {
      handler("Publish message failed");
    } else console.log(packet);
  };

  console.log("========== End OnPublish Event ========== ");
  
  return client.publish(apiEndpoint + topic, message, { qos: 1 }, callBack);
}

function onMessage(client: mqtt.MqttClient, callBack: Function) {
  client.on(
    "message",
    (topic: string, message: Buffer, packet: mqtt.IPublishPacket) => {
      console.log("========== OnMessage Event ========== ");
      console.log("topic :", topic);
      console.log("message :", message);
      const strMess = new TextDecoder("utf-8").decode(message);
      console.log("message as string :", strMess);
      // callBack(JSON.parse(new TextDecoder("utf-8").decode(message))); packet: mqtt.IPublishPacket
      console.log("========== End OnMessage Event ========== ");
    }
  );
}

function unsubscribe(client: mqtt.MqttClient, topic: string) {
  console.log("========== OnUnSubscribe Event ========== ");
  console.log("topic :", topic);
  console.log("========== End OnUnSubscribe Event ========== ");
  client.unsubscribe(apiEndpoint + topic);
}

function closeConnection(client: mqtt.MqttClient) {
  client.end();
}

function onClose(client: mqtt.MqttClient, callBack: Function) {
  client.on("close", () => {
    console.log("========== OnClose Event ========== ");
    callBack();
    console.log("========== End OnClose Event ========== ");
  });
}

function onDisconnect(client: mqtt.MqttClient, callBack: Function) {
  client.on("disconnect", (packet: mqtt.IPacket) => {
    console.log("========== OnDisconnect Event ========== ");
    console.log("packet :", packet);
    console.log("========== End OnDisconnect Event ========== ");
    callBack(packet);
  });
}

const mqttService = {
  getClient,
  subscribe,
  publish,
  onMessage,
  unsubscribe,
  closeConnection,
  onDisconnect,
  onClose,
};

export default mqttService;
