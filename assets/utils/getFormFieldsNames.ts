export function getFormFieldsName(meta: any, excluded?: string[]) {
    const arrNames: string[] = [];
    const excludedArray = excluded ? [...excluded, "schema"] : ["schema"];
    // console.table(excludedArray);

    for (let fieldname in meta) {
        !excludedArray.includes(fieldname) && arrNames.push(meta[fieldname].name);
    }
    
    return [ ...arrNames ];
}