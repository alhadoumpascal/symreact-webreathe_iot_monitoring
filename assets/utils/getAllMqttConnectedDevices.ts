import EnvConfig from "../config/env.config";

const url = `${EnvConfig.API_URL}iot-devices/connected`;

export default async function getConnectedDevices() {
  let connectedDevices: any[] = [];

  try {
    const res = await fetch(url); // Fetch all connected devices on the mqtt server
    // console.log(res);

    if (res.ok) {
      try {
        connectedDevices = await res.json();
        connectedDevices = connectedDevices.map(
          (connection) => connection.variable_map.client_id
        );
      } catch (error) {
        console.log(error);
        return Promise.resolve({
          status: res.status,
          message: res.statusText,
        });
      }
    }
  } catch (error) {
    console.log(error);
    Promise.reject(error);
  }

  return connectedDevices;
}
