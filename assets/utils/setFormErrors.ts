import { ErrorOption } from "react-hook-form";
import { UseFormSetError } from "react-hook-form/dist/types";

type errorType = {
    status: number;
    message: string | any;
    type: string;
}

export const setFormErrors = (error: errorType, setFieldError?: UseFormSetError<any>) => {
    switch (error.type) {
        case "create":
            if (error.status !== 201) {
                if (error.message && typeof error.message !== "string") {
                    
                    error.message.forEach((violation: any) => {
                        setFieldError && setFieldError(violation.propertyPath, {
                            type: "manual",
                            message: violation.message,
                        });
                    });
                }
            }
            
            break;
    
        case "update":
            if (error.status !== 201) {
                if (error.message && typeof error.message !== "string") {
                    
                    error.message.forEach((violation: any) => {
                        setFieldError && setFieldError(violation.propertyPath, {
                            type: "manual",
                            message: violation.message,
                        });
                    });
                }
            }
            
            break;
    
        default:
            break;
    }
}

