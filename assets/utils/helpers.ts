export function capitalize(str: string) {
  return str.charAt(0).toLocaleUpperCase() + str.slice(1);
}

export function average(numbers: number[]): number {
  return numbers.reduce((x, y) => x + y) / numbers.length;
}
