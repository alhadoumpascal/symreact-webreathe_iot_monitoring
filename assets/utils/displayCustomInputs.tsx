import CustomFormControl from "../components/form/CustomFormControl";
import CustomInput, { INPUT_TYPE } from "../components/form/CustomInput";
import { getFormFieldsName } from "./getFormFieldsNames";

/**
 * 
 * @param meta FormField meta data
 * @param register React Hook Form register function
 * @param errors React Hook Form errors object
 * @param isLoading React Query Fetch status
 * @param inputs Array of form field's names to display
 * @param excluded Array of form field's names to hidden
 * @returns 
 */
export default function displayCustomInputs<T>(
  meta: any,
  register: any,
  errors: any,
  isLoading?: boolean,
  inputs?: string[],
  excluded?: string[]
) {
  // const arr = (inputs && inputs?.length > 0) ? inputs : getFormFieldsName(meta, excluded);
  let arr: string[] = [];

  const excludedArray = excluded ? [...excluded, "schema"] : ["schema"];

  if (inputs && inputs?.length > 0) {
    // console.table(excludedArray);

    for (let input of inputs) {
      !excludedArray.includes(input) && arr.push(meta[input as keyof T].name);
    }
  } else arr = getFormFieldsName(meta, excludedArray);

  return arr.map((input, index) => {
    // console.log(input)
    // console.log(errors[meta[input as keyof T].name])
    
    return (
      <div className="col-12" key={index}>
        <CustomFormControl
          inputNameProp={meta.name}
          isLoading={isLoading}
          labelText={meta[input as keyof T].label}
          isInvalid={errors[meta[input as keyof T].name]}
        >
          <CustomInput
            hasError={errors[meta[input as keyof T].name]}
            register={register(meta[input as keyof T].name)}
            meta={meta[input as keyof T]}
          />
        </CustomFormControl>
      </div>
    );
  });
}
