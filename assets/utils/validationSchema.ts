export const getValidationSchema = (param: any): any => {
  const validationSchema: any = {};
  for (let fieldname in param) {
    validationSchema[fieldname] = param[fieldname].validation;
  }
  // console.log(validationSchema);
  return { ...validationSchema };
};

export const getValidationSchema2 = (param: any): any => {
  const validationSchema: any = {};
  console.log('prams: ', param);
  
  for (let fieldname in param) {
    const meta = param as any;
    const field = meta[fieldname];
    validationSchema[field.name] = field.validation;
  }
  // console.log(validationSchema);
  return { ...validationSchema };
};
