import { TDeviceCategoryDto } from "../dto/deviceCategory.dto";
import { TIoTDeviceDto } from "../dto/iotDevice.dto";
import { TCategoryName } from "../types/DeviceCategoryType";
import { I_IoTDeviceType } from "../types/IoTDeviceType";

export class IoTDeviceAdapter {
  /**
   * Transforms the Backend IoT Device object into Frontend IoT Device object
   * @param res Backend IoT Device object
   * @returns Frontend IoT Device object
   */
  static adapt(res: TIoTDeviceDto): I_IoTDeviceType {
    const category = res.deviceCategory as TDeviceCategoryDto;
    return {
      devicePrimaryKey: res.id!,
      deviceName: res.name,
      deviceID: res.deviceId,
      deviceType: (category && category.name as TCategoryName) || "",
      deviceIri: res["@id"]!,
      categoryIri: (category && category["@id"]!) || "",
      createdAt: res.createdAt
    };
  }
  
  /**
   * Transforms the Frontend IoT Device object into Backend IoT Device object
   * @param res Frontend IoT Device object
   * @returns Backend IoT Device object
   */
   static reverse(res: I_IoTDeviceType): TIoTDeviceDto {
    console.log(res);
    return {
      id: res.devicePrimaryKey, 
      name: res.deviceName, 
      deviceId: res.deviceID, 
      deviceCategory: res.categoryIri,
    };
  }

}
