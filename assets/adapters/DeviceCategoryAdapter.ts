import { TDeviceCategoryDto } from "../dto/deviceCategory.dto";
import { IDeviceCategoryType, TCategoryName } from "../types/DeviceCategoryType";
import { IoTDeviceAdapter } from "./IoTDeviceAdapter";

export class DeviceCategryAdapter {
    /**
     * Transforms the Backend IoT Device Category object into Frontend IoT Device Category object
     * @param res Backend IoT Device Category object
     * @returns Frontend IoT Device Category object
     */
    static adapt(res: TDeviceCategoryDto): IDeviceCategoryType {
      // const devices = res.iotDevices as TDeviceCategoryDto;
      console.log(res.ioTDevices);
      return {
        id: res.id!,
        deviceCategoryName: res.name as TCategoryName,
        iri: res["@id"]!,
        ioTDevices: res.ioTDevices?.map(devices => ({...IoTDeviceAdapter.adapt(devices), deviceType: res.name as TCategoryName, categoryIri: res["@id"]!}))!,
      };
    }
    
    /**
     * Transforms the Frontend IoT Device Category object into Backend IoT Device Category object
     * @param res Frontend IoT Device Category object
     * @returns Backend IoT Device Category object
     */
     static reverse(res: IDeviceCategoryType): TDeviceCategoryDto {
      return {
        id: res.id, 
        name: res.deviceCategoryName,
      };
    }
  
  }