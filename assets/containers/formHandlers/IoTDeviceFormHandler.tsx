import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import React from "react";
import {
  FieldValues,
  UseFormGetValues,
  UseFormReset,
  UseFormSetError,
  UseFormSetValue,
} from "react-hook-form";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { IoTDeviceAdapter } from "../../adapters/IoTDeviceAdapter";
import IoTDeviceFormUI from "../../components/formUI/IoTDeviceFormUI";
import IoTDeviceFormLoader from "../../components/loader/IoTDeviceFormLoader";
import EnvConfig from "../../config/env.config";
import { TIoTDeviceDto } from "../../dto/iotDevice.dto";
import * as DeviceCategoriesAPI from "../../services/api/deviceCategoryAPI";
import * as IotDevicesAPI from "../../services/api/iotDevicesAPI";
import { IDeviceCategoryType } from "../../types/DeviceCategoryType";
import { FormSelectInputOptionMeta } from "../../types/FormMetaType";
import { I_IoTDeviceType } from "../../types/IoTDeviceType";
import { setFormErrors } from "../../utils/setFormErrors";
import iotDeviceFormMeta from "./iotDeviceFormMeta";

let selectOptions: FormSelectInputOptionMeta[] = [];

const creationTitle = "Create a new IoT Device";

const IoTDeviceFormHandler = () => {
  const formUIRef = React.useRef<{
    reset: UseFormReset<FieldValues>;
    setValue: UseFormSetValue<FieldValues>;
    getValues: UseFormGetValues<FieldValues>;
    setError: UseFormSetError<FieldValues>;
  }>(null);

  let { deviceId } = useParams();

  const [editing, setEditing] = React.useState<boolean | undefined>(
    deviceId !== "new"
  );

  React.useEffect(() => {
    // console.log("on useEffect editing = ", editing);
    if (deviceId !== "new") {
      setEditing(deviceId !== "new");
    }

    return () => {};
  }, [deviceId]);

  const queryClient = useQueryClient();

  const cachedDevice = React.useMemo(() => {
    if (deviceId !== "new") {
      const devices = queryClient.getQueryData<I_IoTDeviceType[] | []>([
        EnvConfig.IOT_DEVICES_QKEY,
      ]);

      return (
        devices?.filter(
          (device) => String(device.devicePrimaryKey) === String(deviceId)
        )[0] || null
      );
    }
    return null;
  }, [deviceId]);

  // Initialize the react Query service to get all iot device's categories from server

  const cachedCategories = React.useMemo(
    () =>
      queryClient.getQueryData<IDeviceCategoryType[] | []>([
        EnvConfig.DEVICE_CATGO_QKEY,
      ]) || [],
    [deviceId]
  );

  if (cachedCategories.length > 0) {
    for (let i = 0; i < cachedCategories.length; i++) {
      selectOptions.push({
        value: cachedCategories[i].iri,
        label: cachedCategories[i].deviceCategoryName,
        selected: i === 0,
      });
    }
  }

  const [meta, setMeta] = React.useState({
    ...iotDeviceFormMeta,
    deviceCategory: {
      ...iotDeviceFormMeta.deviceCategory,
      options: selectOptions,
    },
  });

  const { isLoading: isLoadingDeviceCategories } = useQuery({
    queryKey: [EnvConfig.DEVICE_CATGO_QKEY],
    queryFn: async () => await DeviceCategoriesAPI.getAll(),
    refetchOnReconnect: true,
    refetchOnWindowFocus: false,
    refetchInterval: false,
    refetchIntervalInBackground: true,
    refetchOnMount: true,
    enabled: !(cachedCategories.length > 0),
    onSuccess: (_deviceCategories: IDeviceCategoryType[]) => {
      selectOptions = [];
      for (let i = 0; i < _deviceCategories.length; i++) {
        selectOptions.push({
          value: _deviceCategories[i].iri,
          label: _deviceCategories[i].deviceCategoryName,
          selected: i === 0,
        });
      }

      // console.log(selectOptions);
      const _meta = { ...meta };

      _meta.deviceCategory.options = selectOptions;

      setMeta(_meta);
    },
    onError: (err) => {
      toast.error(
        "An error occurred while retrieving the list of IoT Device Categories."
      );
    },
  });

  // react hook form set the category select value to the first option to avoid some bug in case of creating iot device
  React.useEffect(() => {
    if (
      !editing &&
      (cachedCategories.length > 0 || !isLoadingDeviceCategories)
    ) {
      const _options = meta.deviceCategory?.options;

      _options &&
        _options.length > 0 &&
        formUIRef.current?.setValue(
          meta.deviceCategory.name,
          _options[0].value
        );
    }
  }, [meta]);

  const initFormInputValues = (iotDevice: I_IoTDeviceType) => {
    Object.keys(formUIRef?.current?.getValues()!).forEach((field: any) => {
      const iotDevice_: TIoTDeviceDto = IoTDeviceAdapter.reverse(iotDevice);

      const field_ = field as keyof TIoTDeviceDto;

      formUIRef.current?.setValue(field_, iotDevice_[field_]);
    });
    // setMeta(meta_);
  };

  //   Set react Query to GET IoT Devices Data Collection
  const queryKey = [editing ? "IoTDevice." + deviceId : "IoTDevice"];
  const { data: iotDeviceData, isLoading: isLoadingIotDevice } = useQuery({
    queryKey: queryKey,
    enabled: !(cachedDevice !== null) && editing,
    refetchOnReconnect: true,
    refetchOnWindowFocus: false,
    refetchInterval: 120000,
    refetchIntervalInBackground: true,
    refetchOnMount: false,
    queryFn: async () => {
      return await IotDevicesAPI.getById(deviceId!);
    },
    onSuccess: (iotDevice_: I_IoTDeviceType) => {
      console.log(iotDevice_);
    },
    onError: (data_: any) => {
      toast.error(
        "An unexpected error occured when trying to get the data. 😭"
      );
    },
  });

  // Set the form fields value with the iot device information

  React.useEffect(() => {
    if (cachedDevice !== null) {
      initFormInputValues(cachedDevice);
    }
    if (iotDeviceData) {
      initFormInputValues(iotDeviceData);
    }
  }, [iotDeviceData]);

  // Set React Query mutation to create a new IoT Device
  const { mutateAsync: createMutation, isLoading: isCreateSubmitting } =
    useMutation({
      mutationFn: async (iotDevice: TIoTDeviceDto) => {
        return await IotDevicesAPI.add(iotDevice);
      },
      onSuccess: (data: TIoTDeviceDto) => {
        // Invalidate the iot devices query
        queryClient.invalidateQueries([EnvConfig.IOT_DEVICES_QKEY]);
        queryClient.setQueryData<I_IoTDeviceType[] | []>(
          [EnvConfig.IOT_DEVICES_QKEY],
          (devices) => {
            devices?.push(IoTDeviceAdapter.adapt(data) as never);
            return devices;
          }
        );

        formUIRef.current?.reset(); // Reset IoT Device (Edit or Update) Form
        console.log("Response: ", data);
        toast.success("Successfully created the IoT Device. 😃"); // Display successfull toast message
      },
      onError: (err: any) => {
        toast.error("The IoT Device creation failed. 😭"); // Display error toast message
        console.log("Error: ", err);
        err.type = "create";
        setFormErrors(err, formUIRef?.current?.setError);
      },
    });

  // Set React Query mutation to update an IoT Device
  const { mutateAsync: updateMutation, isLoading: isUpdateSubmitting } =
    useMutation({
      mutationFn: async (iotDevice: TIoTDeviceDto) => {
        return await IotDevicesAPI.update(iotDevice);
      },
      onSuccess: (data: TIoTDeviceDto) => {
        queryClient.setQueryData<I_IoTDeviceType[] | []>(
          [EnvConfig.IOT_DEVICES_QKEY],
          (devices) => {
            return devices?.map((device) =>
              device.devicePrimaryKey === data.id
                ? IoTDeviceAdapter.adapt(data)
                : device
            );
          }
        );
        console.log("Response: ", data);
        toast.success("Successfully updated the IoT Device. 😃");
        // formUIRef.current?.reset();
      },
      onError: (err: any) => {
        toast.error("The IoT Device modification failed. 😭");
        console.log("Error: ", err);
        err.type = "create";
        setFormErrors(err, formUIRef?.current?.setError);
      },
    });

  const onSubmit = async (payload: TIoTDeviceDto) => {
    console.log("payload: ", payload);
    try {
      if (editing) {
        const updateDevice = { ...payload, id: deviceId };
        await updateMutation(updateDevice);
      } else {
        await createMutation(payload);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const isSubmitting = isCreateSubmitting || isUpdateSubmitting;

  const isLoading = editing
    ? !cachedDevice && isLoadingIotDevice
    : !(cachedCategories.length > 0) && isLoadingDeviceCategories;

  return (
    <>
      {isLoading ? (
        <IoTDeviceFormLoader />
      ) : (
        <IoTDeviceFormUI
          title={
            editing
              ? `${
                  "Modification of the IoT Device : " +
                  (iotDeviceData?.deviceName || "")
                }`
              : creationTitle
          }
          meta={meta}
          onSubmit={onSubmit}
          isLoading={isLoading}
          isSubmitting={isSubmitting}
          ref={formUIRef}
        />
      )}
    </>
  );
};

export default IoTDeviceFormHandler;
