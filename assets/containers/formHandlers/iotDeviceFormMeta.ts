import * as yup from "yup";
import { FormInputMeta } from "../../types/FormMetaType";
import { getValidationSchema } from "../../utils/validationSchema";


export interface I_IoTDeviceFormMeta {
  name: FormInputMeta;
  deviceId: FormInputMeta;
  deviceCategory: FormInputMeta;
  
  schema: any;
}

const iotDeviceFormMeta = {} as I_IoTDeviceFormMeta;

iotDeviceFormMeta.name = {
  name: "name",
  label: "IoT Device Name",
  type: "text",
  validation: yup.string().min(3, "IoT Device Name must be between 3 and 100 characters long").max(100, "IoT Device Name must be between 3 and 100 characters long").required("IoT Device Name is required !"),
};

iotDeviceFormMeta.deviceId = {
  name: "deviceId",
  label: "IoT Device ID",
  type: "text",
  validation: yup.string().min(3, "IoT Device ID must be between 3 and 100 characters long").max(100, "IoT Device ID must be between 3 and 100 characters long").required("IoT Device ID is required !"),
};

iotDeviceFormMeta.deviceCategory = {
  name: "deviceCategory",
  label: "IoT Device Category",
  type: "select",
  validation: yup.string().required("The IoT Device must belong to a Category !"),
};

iotDeviceFormMeta.schema = yup.object().shape(getValidationSchema(iotDeviceFormMeta));

export default iotDeviceFormMeta;
