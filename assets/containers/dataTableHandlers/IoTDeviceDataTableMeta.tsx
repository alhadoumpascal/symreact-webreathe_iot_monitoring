import { createColumnHelper } from "@tanstack/react-table";
import CustomTableBtn, {
  TDataTableRowAction,
} from "../../components/table/CustomTableBtn";
import { BSColor } from "../../types/BootstrapType";
import { I_IoTDeviceType } from "../../types/IoTDeviceType";

type Props = {
  isLoading: boolean | undefined;
  isDeleteLoading: boolean;
  deleteActiveBtnId: number | string;
  handleAction: (row: any, action: TDataTableRowAction) => void;
};

const columnHelper = createColumnHelper<I_IoTDeviceType>();

type ActionBtnType = {
  action: TDataTableRowAction;
  variant: BSColor;
};

const actionBtnObj: ActionBtnType[] = [
  {
    action: "Edit",
    variant: "primary",
  },
  // {
  //   action: "Detail",
  //   variant: "info",
  // },
  {
    action: "Delete",
    variant: "danger",
  },
];

export function getIoTDeviceDataTableColumns({
  isLoading,
  isDeleteLoading,
  deleteActiveBtnId,
  handleAction,
}: Props) {
  return [
    columnHelper.accessor("devicePrimaryKey", {
      id: "devicePrimaryKey",
      header: "#",
      cell: (props) => {
        return (
          // TODO: Design skeleton
          <span className="text-center">{props.getValue()}</span>
        );
      },
    }),

    columnHelper.accessor("deviceName", {
      id: "deviceName",
      header: "Name",
      cell: (props) => {
        return (
          // TODO: Design skeleton
          props.getValue()
        );
      },
    }),

    columnHelper.accessor("deviceID", {
      id: "deviceID",
      header: "Device ID",
      cell: (props) => {
        return (
          // TODO: Design skeleton
          props.getValue()
        );
      },
    }),

    columnHelper.accessor("deviceType", {
      id: "deviceType",
      header: "Type",
      cell: (props) => {
        return (
          // TODO: Design skeleton
          props.getValue()
        );
      },
    }),

    columnHelper.display({
      id: "action",
      header: "",
      cell: (props) => {
        const isMutationBtn =
          isDeleteLoading &&
          deleteActiveBtnId === props.row.original.devicePrimaryKey;

        return (
          <div className="d-flex gap-3">
            {actionBtnObj.map((btn, key) =>
              btn.action === "Delete" ? (
                <CustomTableBtn
                  key={key}
                  isLoading={isLoading}
                  variant={btn.variant}
                  action={btn.action}
                  size={"sm"}
                  onClick={() => {
                    if (
                      confirm(
                        `Do you want to delete ${props.row.original.deviceName}(#${props.row.original.deviceID}) ?`
                      ) == true
                    ) {
                      handleAction(props.row, btn.action);
                    } else {
                      console.log("Delete Cancelled!");
                    }
                  }}
                  isSubmitting={isMutationBtn}
                  disabled={isMutationBtn}
                />
              ) : (
                <CustomTableBtn
                  key={key}
                  isLoading={isLoading}
                  variant={btn.variant}
                  action={btn.action}
                  size={"sm"}
                  onClick={() => handleAction(props.row, btn.action)}
                  disabled={isDeleteLoading}
                />
              )
            )}
          </div>
        );
      },
    }),
  ];
}
