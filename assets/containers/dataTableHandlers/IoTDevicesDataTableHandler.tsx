import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import React from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import CustomButton from "../../components/form/CustomButton";
import IoTDeviceDataTableLoader from "../../components/loader/IoTDevicesDataTableLoader";
import CustomReactTable from "../../components/table/CustomReactTable";
import { TDataTableRowAction } from "../../components/table/CustomTableBtn";
import EnvConfig from "../../config/env.config";
import URLConfig from "../../config/urls/url.config";
import * as IotDevicesAPI from "../../services/api/iotDevicesAPI";
import { I_IoTDeviceType } from "../../types/IoTDeviceType";
import { getIoTDeviceDataTableColumns } from "./IoTDeviceDataTableMeta";

const IoTDevicesDataTableHandler = () => {
  const [deleteActiveBtnId, setDeleteActiveBtnId] = React.useState<
    number | string
  >(0);

  const navigate = useNavigate();

  //   Set react Query to GET IoT Devices Data Collection
  const { data, isLoading, status } = useQuery({
    queryKey: [EnvConfig.IOT_DEVICES_QKEY],
    queryFn: async () => {
      return await IotDevicesAPI.getAll();
    },
    refetchOnReconnect: true,
    refetchOnWindowFocus: false,
    refetchInterval: 120000,
    refetchIntervalInBackground: true,
    refetchOnMount: true,
    onSuccess: (data_: any) => {
      // console.log(data_);
    },
    onError: (data_: any) => {
      toast.error(
        "An unexpected error occured when trying to get the data. 😭"
      );
    },
  });

  const queryClient = useQueryClient();

  // Set React Query mutation to delete an IoT Device
  const { mutateAsync: deleteMutation, isLoading: isDeleteLoading } =
    useMutation({
      mutationFn: async (deviceId: number | string) => {
        return await IotDevicesAPI.remove(deviceId);
      },
      onSuccess: (_) => {
        // Update the global state management for the iot devices data
        const updateData = queryClient.setQueryData<I_IoTDeviceType[]>(
          [EnvConfig.IOT_DEVICES_QKEY],
          (iotDevicesData) => {
            return (
              iotDevicesData &&
              iotDevicesData.filter(
                (device) => device.devicePrimaryKey !== deleteActiveBtnId
              )
            );
          }
        );
        console.log(updateData);

        toast.success("Successfully deleted the IoT Device. 😃"); // Display successfull toast message
      },
      onError: (error) => {
        toast.error("The removal of the IoT Device failed. 😭"); // Display error toast message
        console.log("Error: ", error);
      },
    });

  /**
   * Handle the click event on the row datatable action button
   *
   * @param row iot device row datatable
   * @param action datatable action button. Possible value : "Edit", "Delete" or "Detail"
   */
  const handleAction = async (row: any, action: TDataTableRowAction) => {
    // setAction(action);
    console.log(action, "Clicked");
    setDeleteActiveBtnId(row.original.devicePrimaryKey);
    switch (action) {
      case "Edit":
        navigate(
          URLConfig.IOT_DEVICE.ROOT + "/" + row.original.devicePrimaryKey
        );
        break;

      case "Delete":
        await deleteMutation(row.original.devicePrimaryKey);
        break;

      case "Detail":
        // TODO: handle show iot device history
        break;

      default:
    }
  };

  const columns = getIoTDeviceDataTableColumns({
    isLoading,
    isDeleteLoading,
    deleteActiveBtnId,
    handleAction,
  });

  /**
   * Handle the click event on the add new iot device button
   */
  const handleAddBtnClick = () => {
    navigate(URLConfig.IOT_DEVICE.CREATE);
  };

  // Get the datatable data to display
  // Setting the customers fetching status message
  let content =
    status === "loading" ? (
      <IoTDeviceDataTableLoader /> // <h2>Loading...</h2>
    ) : status === "error" ? (
      // <h2>Error: {error.message}</h2>
      <h2>
        Oops, Sorry we can't retrieve iot devices list, please try to refresh
        the page again...
      </h2>
    ) : null;
  const iotDevicesData = isLoading ? [] : data;
  // console.log(iotDevicesData);

  return (
    <>
      {/* {isLoading ? (
        <IoTDeviceDataTableLoader />
      ) : ( */}
      <div className="row gap-4 mt-6">
        <div className="col-12 d-flex justify-content-end">
          {!isLoading && (
            <CustomButton
              loading={isLoading}
              size={"lg"}
              onClick={handleAddBtnClick}
            >
              Add new Device
            </CustomButton>
          )}
        </div>
        <div className="col-12">
          {/* <CustomReactTable data={iotDevicesData} columns={columns} /> */}
          {content !== null ? (
            content
          ) : data && data.length > 0 ? (
            <CustomReactTable data={iotDevicesData} columns={columns} />
          ) : (
            <h2>No data to display, please add a new iot device</h2>
          )}
        </div>
      </div>
      {/* )} */}
    </>
  );
};

export default IoTDevicesDataTableHandler;
