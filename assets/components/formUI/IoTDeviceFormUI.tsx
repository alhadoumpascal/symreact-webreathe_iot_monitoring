import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import URLConfig from "../../config/urls/url.config";
import { I_IoTDeviceFormMeta } from "../../containers/formHandlers/iotDeviceFormMeta";
import { TFormUIProps } from "../../types/FormUIPropsType";
import displayCustomInputs from "../../utils/displayCustomInputs";
import CustomButton from "../form/CustomButton";
import CustomSaveBtn from "../form/CustomSaveBtn";

type Props = {
  meta: I_IoTDeviceFormMeta;
  title?: string;
} & TFormUIProps;

const IoTDeviceFormUI = React.forwardRef(
  ({ onSubmit, isLoading, isSubmitting, meta, title }: Props, ref: any) => {
    const {
      register,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
      getValues,
      setError,
    } = useForm({ resolver: yupResolver(meta.schema) });

    React.useImperativeHandle(ref, () => ({
      reset,
      setValue,
      getValues,
      setError,
    }));

    const navigate = useNavigate();

    return (
      <form className="" onSubmit={handleSubmit(onSubmit)} ref={ref}>
        <h1>{title}</h1>
        <div className="row my-4">
          {displayCustomInputs<I_IoTDeviceFormMeta>(
            meta,
            register,
            errors,
            isLoading
          )}
        </div>

        <div className="my-6 gap-4 d-flex justify-content-end">
          <CustomSaveBtn isLoading={isLoading} isSubmitting={isSubmitting} />
          <CustomButton
            actionType="submit"
            disabled={isSubmitting}
            onClick={() => navigate(URLConfig.IOT_DEVICE.ROOT)}
          >
            Back to list
          </CustomButton>
        </div>
      </form>
    );
  }
);

export default IoTDeviceFormUI;
