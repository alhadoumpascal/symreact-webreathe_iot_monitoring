import React from "react";
import ContentLoader from "react-content-loader";

const DashboardLoader = () => (
  // height = 1975
  <ContentLoader
    width={1320}
    height={1975}
    viewBox="0 0 1320 1975"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
  >
    <rect x="2" y="20" rx="6" ry="6" width="1294" height="52" />
    <rect x="17" y="92" rx="6" ry="6" width="554" height="590" />
    <rect x="663" y="92" rx="6" ry="6" width="554" height="590" />

    <rect x="2" y="782" rx="6" ry="6" width="1294" height="52" />
    <rect x="17" y="854" rx="6" ry="6" width="554" height="590" />
    <rect x="663" y="854" rx="6" ry="6" width="554" height="590" />
  </ContentLoader>
);

export default DashboardLoader;
