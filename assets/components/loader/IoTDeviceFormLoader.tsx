import React from "react";
import ContentLoader from "react-content-loader";

const IoTDeviceFormLoader = () => (
  <ContentLoader
    speed={0.8}
    width={1320}
    height={464}
    viewBox="0 0 1320 464"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
  >
    <rect x="66" y="90" rx="0" ry="0" width="615" height="48" />
    <rect x="66" y="175" rx="6" ry="6" width="1270" height="38" />
    <rect x="67" y="248" rx="6" ry="6" width="1270" height="38" />
    <rect x="67" y="337" rx="6" ry="6" width="1270" height="38" />
    <rect x="1100" y="423" rx="9" ry="9" width="89" height="38" />
    <rect x="1215" y="423" rx="9" ry="9" width="102" height="38" />
  </ContentLoader>
);

export default IoTDeviceFormLoader;
