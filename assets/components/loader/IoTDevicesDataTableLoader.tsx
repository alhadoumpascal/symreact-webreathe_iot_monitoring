import React from "react";
import ContentLoader from "react-content-loader";

const IoTDeviceDataTableLoader = () => {
  return (
    <ContentLoader
      width={1320}
      height={484}
      viewBox="0 0 1320 484"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
    >
      <rect x="1118" y="23" rx="6" ry="6" width="177" height="48" />

      {/* Column 1 */}
      <rect x="2" y="140" rx="" ry="2" width="20" height="19" />
      <rect x="2" y="197" rx="" ry="2" width="25" height="19" />
      <rect x="2" y="259" rx="" ry="2" width="25" height="19" />
      <rect x="2" y="317" rx="" ry="2" width="25" height="19" />
      <rect x="2" y="380" rx="" ry="2" width="25" height="19" />

      {/* Column 2 */}
      <rect x="70" y="141" rx="10" ry="10" width="299" height="19" />
      <rect x="70" y="198" rx="10" ry="10" width="299" height="19" />
      <rect x="70" y="260" rx="10" ry="10" width="299" height="19" />
      <rect x="70" y="318" rx="10" ry="10" width="299" height="19" />
      <rect x="70" y="381" rx="10" ry="10" width="299" height="19" />

      {/* Column 3 */}
      <rect x="447" y="140" rx="10" ry="10" width="120" height="19" />
      <rect x="447" y="197" rx="10" ry="10" width="120" height="19" />
      <rect x="447" y="259" rx="10" ry="10" width="120" height="19" />
      <rect x="447" y="317" rx="10" ry="10" width="120" height="19" />
      <rect x="447" y="380" rx="10" ry="10" width="120" height="19" />

      {/* Column 4 */}
      <rect x="624" y="141" rx="10" ry="10" width="269" height="19" />
      <rect x="624" y="198" rx="10" ry="10" width="269" height="19" />
      <rect x="624" y="260" rx="10" ry="10" width="269" height="19" />
      <rect x="624" y="318" rx="10" ry="10" width="269" height="19" />
      <rect x="624" y="381" rx="10" ry="10" width="269" height="19" />

      {/* Column 5 */}
      <rect x="951" y="139" rx="2" ry="2" width="45" height="19" />
      <rect x="951" y="196" rx="2" ry="2" width="45" height="19" />
      <rect x="951" y="258" rx="2" ry="2" width="45" height="19" />
      <rect x="951" y="316" rx="2" ry="2" width="45" height="19" />
      <rect x="951" y="379" rx="2" ry="2" width="45" height="19" />

      {/* Column 6 */}
      <rect x="1011" y="138" rx="2" ry="2" width="45" height="19" />
      <rect x="1011" y="195" rx="2" ry="2" width="45" height="19" />
      <rect x="1011" y="257" rx="2" ry="2" width="45" height="19" />
      <rect x="1011" y="315" rx="2" ry="2" width="45" height="19" />
      <rect x="1011" y="378" rx="2" ry="2" width="45" height="19" />
    </ContentLoader>
  );
};

export default IoTDeviceDataTableLoader;
