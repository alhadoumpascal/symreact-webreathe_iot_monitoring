import { FormInputMeta } from "../../types/FormMetaType";
import CustomFormErrorMessage from "./CustomFormErrorMessage";

type Props = {
  meta: FormInputMeta;
  id: string;
  register?: any;
  hasError?: any;
};

export default function CustomTextInput({
  meta,
  id,
  register,
  hasError,
  ...rest
}: Props) {
  
  return (
    <>
      {register ? (
        <>
            <input
            {...rest}
            type={meta?.type}
            value={meta?.value}
            className={`form-control ${hasError !== undefined ? (!!hasError ? "is-invalid" : "is-valid") : "" } `}
            id={id}
            placeholder={meta?.placeholder}
            disabled={meta?.disabled}
            {...register}
            />
            <CustomFormErrorMessage hasError={hasError} />
        </>
      ) : (
        <input
        type={meta?.type}
        name={meta?.name}
        value={meta?.value}
        onChange={meta.onChange}
        className={`form-control ${hasError !== undefined ? (!!hasError ? "is-invalid" : "is-valid") : "" } `}
        id={id}
        placeholder={meta?.placeholder}
        disabled={meta?.disabled}
        {...rest}
        />
      )}
    </>
  );
}
