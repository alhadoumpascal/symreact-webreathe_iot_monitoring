import React from 'react';
import { FaSave } from 'react-icons/fa';
import CustomButton from './CustomButton';

type Props = {
  isLoading?: boolean;
  isSubmitting?: boolean;
  [x: string]: any;
};

const CustomSaveBtn: React.FC<Props> = ({
  isLoading,
  isSubmitting,
  ...rest
}) => {
  return (
    // <CC.Skeleton isLoaded={!(!!isLoading)}>
      <CustomButton {...rest} loading={isSubmitting} variant={"success"} actionType={'submit'}>
        <FaSave style={{ marginRight: '10px' }} /> Save
      </CustomButton>
    // </CC.Skeleton>
  );
};

export default CustomSaveBtn;
