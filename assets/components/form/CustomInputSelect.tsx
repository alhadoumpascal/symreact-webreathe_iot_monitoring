
import { FormInputMeta } from "../../types/FormMetaType";
import CustomFormErrorMessage from "./CustomFormErrorMessage";

type Props = {
  meta: FormInputMeta;
  [x: string]: any;
  register?: any;
  hasError?: any;
};

export default function CustomInputSelect({
  meta,
  register,
  hasError,
  ...rest
}: Props) {
  const selectedOpt = meta.options && meta.options.find((opt) => opt.selected);
  const { name } = meta;

  return (
    <>
      {register ? (
        <>
          <select
            placeholder={meta.placeholder || "Please select a value"}
            {...rest}
            {...register}
            className={`form-control form-select ${hasError !== undefined ? (!!hasError ? "is-invalid" : "is-valid") : "" } `}
            // value={selectedOpt ? selectedOpt.value : ''}
            id={name}
          >
            {meta.options &&
              meta.options.map((opt, i) => (
                <option key={i} value={opt.value}>
                  {opt.label}
                </option>
              ))}
          </select>
          <CustomFormErrorMessage hasError={hasError} />
        </>
      ) : (
        <select
          placeholder={meta.placeholder || "Please select a value"}
          {...rest}
          name={name}
          value={selectedOpt ? selectedOpt.value : ""}
          onChange={meta?.onChange}
          className={`form-control form-select ${hasError !== undefined ? (!!hasError ? "is-invalid" : "is-valid") : "" } `}
          id={name}
        >
          {meta.options &&
            meta.options.map((opt, i) => (
              <option key={i} value={opt.value}>
                {opt.label}
              </option>
            ))}
        </select>
      )}
    </>
  );
}
