
export type TCustomFormControlProps = {
  children: React.ReactNode;
  inputNameProp: string;
  labelText?: string;
  isInvalid?: boolean;
  isLoading?: boolean;
  hasLoader?: boolean;
  // errors?: any,
  labelStyle?: any;
  helperText?: string;
};

export default function CustomFormControl({
  children,
  inputNameProp,
  labelText,
  isInvalid,
  isLoading,
  hasLoader = true,
  labelStyle,
  ...rest
}: TCustomFormControlProps) {
  // console.log(typeof isInvalid === "boolean" ? (isInvalid ? "has-success" : "has-danger") : "undefined" );
  return (
    <>
      {/* <CC.FormControl mb={{ base: '2' }} {...rest} >
      {label && (
        <CC.Skeleton isLoaded={hasLoader && !isLoading} w={'95%'}>
          <CC.FormLabel {...labelStyle}>{label}</CC.FormLabel>
        </CC.Skeleton>
      )}
      <CC.Skeleton isLoaded={hasLoader && !isLoading}></CC.Skeleton>
    </CC.FormControl> */}
      
      <div {...rest} className={`form-group ${isInvalid !== undefined ? (!!isInvalid ? "has-danger" : "has-success") : "" }`}>
        {labelText && (
          <label className="form-label mt-4" htmlFor={inputNameProp}>
            {labelText}
          </label>
        )}
        {children}
      </div>
    </>
  );
}
