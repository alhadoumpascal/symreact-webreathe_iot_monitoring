import React from "react";
import { BSColor, BSSize } from "../../types/BootstrapType";

type BtnProps = {
  children: React.ReactNode;
  actionType?: "button" | "submit";
  loading?: boolean;
  size?: Omit<BSSize, "md" | "xl">;
  variant?: BSColor;
  outline?: boolean;
  [x: string]: any;
};

const CustomButton = ({
  children,
  actionType,
  loading,
  variant,
  size,
  outline = false,
  ...rest
}: BtnProps) => {
  const btnType = actionType || "button";

  const variant_   = variant || "primary";
  const btnVariant = `btn-${!outline ? variant_ : "outline-" + variant_}`;

  const btnSize = size ? "btn-" + size : "";
  return (
    <>
      <button
        type={btnType}
        disabled={!!loading}
        className={`btn ${btnVariant} ${btnSize}`}
        {...rest}
      >
        {loading ? (
          <>
            <span
              className="spinner-border spinner-border-sm me-2"
              role="status"
              aria-hidden="true"
            ></span>
            Loading...
          </>
        ) : children}
      </button>
    </>
  );
};

export default CustomButton;
