
import { FormInputMeta } from '../../types/FormMetaType';
import CustomInputSelect from './CustomInputSelect';
import CustomTextInput from './CustomTextInput';

export enum INPUT_TYPE {
  SELECT = 'select',
  CHECKBOX = 'checkbox',
  RADIO = 'radio',
}

type Props = {
  meta: FormInputMeta;
  register?: any;
  hasError?: any;
};

export default function CustomInput({
  meta,
  register,
  hasError,
  ...rest
}: Props) {
  // console.log(hasError)
  if (meta.type === INPUT_TYPE.SELECT) {
    return (
      <CustomInputSelect {...rest} meta={meta} id={meta.name} register={register} hasError={hasError} />
    );
  }

  return (
    <CustomTextInput
      {...rest}
      id={meta.name}
      meta={meta}
      register={register}
      hasError={hasError}
    />
  );
}
