import React from "react";

type Props = {
  hasError: any;
};

const CustomFormErrorMessage: React.FC<Props> = ({ hasError }) => {
  return (
    <>
      {hasError && (
        <div className="invalid-feedback">{hasError.message as string}</div>
      )}
    </>
  );
};

export default CustomFormErrorMessage;
