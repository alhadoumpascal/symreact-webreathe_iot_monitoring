import { FiEdit3, FiTrash } from "react-icons/fi";
import { FaTrash } from "react-icons/fa";
import { BsTrashFill, BsTrash3, BsEye } from "react-icons/bs";
import CustomButton from "../form/CustomButton";
import { BSColor } from "../../types/BootstrapType";

export type TDataTableRowAction = "Edit" | "Delete" | "Detail";

type TableBtnProps = {
  isLoading?: boolean;
  isSubmitting?: boolean;
  variant?  : BSColor;
  action    : TDataTableRowAction;
  [x: string]: any;
};

const icons = {
  Edit: <FiEdit3 fontSize={"20px"} />,
  Delete: <BsTrashFill fontSize={"20px"} />,
  Detail: <BsEye fontSize={"20px"} />,
};

const CustomTableBtn: React.FC<TableBtnProps> = ({
  isLoading,
  isSubmitting,
  variant,
  action,
  ...rest
}) => {
  return (
    // TODO: Design skeleton
    <CustomButton
      outline
      variant={variant}
      aria-label={`DataTable row ${action}`}
      {...rest}
      loading={isSubmitting}
    >
      {icons[action]}
    </CustomButton>
  );
};

export default CustomTableBtn;
