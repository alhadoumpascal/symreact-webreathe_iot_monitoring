import React from 'react'
// import { ChakraComponents as CC } from '@Modules/core/lib/chakra';
// import { IconButton } from '@chakra-ui/react';
// import { FaAngleDoubleLeft, FaAngleDoubleRight, FaAngleLeft, FaAngleRight } from "react-icons/fa";
import { Table } from '@tanstack/react-table';

type Props = {
    table: Table<any>,
    data: any[],
}
const CustomPagination: React.FC<Props> = ({ table, data }) => {

    const { pageSize, pageIndex } = table.getState().pagination;
    
    return (
        <>
            <div>
                <ul className="pagination">
                    <li className="page-item disabled">
                        <a className="page-link" href="#">&laquo;</a>
                    </li>
                    <li className="page-item active">
                        <a className="page-link" href="#">1</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">2</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">3</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">4</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">5</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">&raquo;</a>
                    </li>
                </ul>
            </div>
            {/* <CC.Flex justifyContent="end" my={8} alignItems="center">
                <CC.Flex>
                    <CC.Tooltip label="First Page">
                    <IconButton
                        onClick={() => table.setPageIndex(0)}
                        isDisabled={!table.getCanPreviousPage()}
                        icon={<FaAngleDoubleLeft width={3} height={3} />}
                        mr={4}
                        aria-label="Go to first page arrow button"
                    />
                    </CC.Tooltip>
                    <CC.Tooltip label="Previous Page">
                    <IconButton
                        onClick={table.previousPage}
                        isDisabled={!table.getCanPreviousPage()}
                        icon={<FaAngleLeft height={6} width={6} />}
                        aria-label="Go to previous page arrow button"
                    />
                    </CC.Tooltip>
                </CC.Flex>
        
                <CC.Flex alignItems="center" mx={5}>
                    <CC.Text flexShrink="0" mr={"5"}>
                    Page{" "}
                    <CC.Text fontWeight="bold" as="span">
                        {pageIndex + 1}
                    </CC.Text>{" "}
                    de{" "}
                    <CC.Text fontWeight="bold" as="span">
                        {table.getPageOptions().length}
                    </CC.Text>
                    </CC.Text>
                    
                    <select
                    w={32}
                    value={pageSize}
                    onChange={(e) => {
                        table.setPageSize(Number(e.target.value));
                    }}
                    >
                    {[5, 10, 25, 50, { label: "All", value: data.length },].map((pageSize, index) => {
                        const size = typeof pageSize === "number" ? pageSize : pageSize.value;
                        return (<option key={index} value={size}>
                        Show {typeof pageSize === "number" ? pageSize : pageSize.label }
                        </option>)
                    })}
                    </select>
                </CC.Flex>
        
                <CC.Flex>
                    <CC.Tooltip label="Next Page">
                    <IconButton
                        onClick={table.nextPage}
                        isDisabled={!table.getCanNextPage()}
                        icon={<FaAngleRight height={6} width={6} />}
                        aria-label="Go to next page arrow button"
                    />
                    </CC.Tooltip>
                    <CC.Tooltip label="Last Page">
                    <CC.IconButton
                        onClick={() => table.setPageIndex(table.getPageCount() - 1)}
                        isDisabled={!table.getCanNextPage()}
                        icon={<FaAngleDoubleRight height={3} width={3} />}
                        ml={4}
                        aria-label="Go to last page arrow button"
                    />
                    </CC.Tooltip>
                </CC.Flex>
            </CC.Flex> */}
        </>
    )
}

export default CustomPagination