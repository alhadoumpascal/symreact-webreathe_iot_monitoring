import mqtt, { MqttClient } from "mqtt";
import React from "react";
import EnvConfig from "../config/env.config";
import {
  TClimateDeviceDataDto,
  TSmartEnergyDeviceDataDto,
} from "../dto/iotDevice.dto";
import { IoTDevicePrefix, I_IoTDeviceType } from "../types/IoTDeviceType";
import { faker } from "@faker-js/faker";
import moment, { duration } from "moment";
import { TMqttWsMessage } from "../types/MqttWsType";
import { saveData } from "../services/api/iotDevicesAPI";
import { useMutation } from "@tanstack/react-query";

type IoTDeviceSimulatorProps = {
  iotDevice: I_IoTDeviceType;
};

let mqttWsClient: MqttClient;

type TIoTDeviceFakeDataConfig = {
  CLIMATE: {
    temperature: number[];
    humidity: number[];
  };
  "SMART ENERGY METER": {
    activePower: number[];
    activeEnergy: number[];
    voltage: number[];
  };
  LIGHT: {
    state: boolean;
  };
  [x: string]: any;
};

const iotDeviceFakeDataConfig: TIoTDeviceFakeDataConfig = {
  CLIMATE: {
    temperature: [20, 28],
    humidity: [77, 98],
  },
  "SMART ENERGY METER": {
    activePower: [0, 20],
    activeEnergy: [0, 20],
    voltage: [201, 230.7],
  },
  LIGHT: {
    state: false,
  },
};

type TInstantaneousData = {
  workingTime: string;
  nbDataSent: number;
  counter: number;
  [x: string]: any;
} & (Partial<TClimateDeviceDataDto> | Partial<TSmartEnergyDeviceDataDto>);

type TIoTDeviceInstantaneousData = {
  CLIMATE: {
    temperature: number;
    humidity: number;
  };
  "SMART ENERGY METER": {
    activePower: number;
    activeEnergy: number;
    voltage: number;
  };
  LIGHT: {
    [x: string]: any;
  };
};

const ioTDeviceInstantaneousData: TIoTDeviceInstantaneousData = {
  CLIMATE: {
    temperature: 0,
    humidity: 0,
  },
  "SMART ENERGY METER": {
    activePower: 0,
    activeEnergy: 0,
    voltage: 0,
  },
  LIGHT: {
    state: false,
  },
};

type TIotDevicesData = (TClimateDeviceDataDto | TSmartEnergyDeviceDataDto) & {
  [x: string]: any;
};

type TIotDeviceData = {
  CLIMATE: TClimateDeviceDataDto;
  "SMART ENERGY METER": TSmartEnergyDeviceDataDto;
  [x: string]: any;
};

const iotDeviceData: TIotDeviceData = {
  CLIMATE: {
    temperature: 0,
    humidity: 0,
    dateTime: "",
    iotDevice: "",
  },
  "SMART ENERGY METER": {
    activePower: 0,
    activeEnergy: 0,
    voltage: 0,
    dateTime: "",
    iotDevice: "",
  },
};

const mqttMessObject: TMqttWsMessage = {
  from: "Devices",
  to: "Monitor",
  subject: "Data",
  message: "",
};

const IoTDeviceSimulator: React.FC<IoTDeviceSimulatorProps> = ({
  iotDevice,
}) => {
  const [instantaneousData, setInstantaneousData] =
    React.useState<TInstantaneousData>({
      // Common data
      workingTime: moment(
        faker.date.between({
          from: moment(iotDevice.createdAt).format(),
          to: moment().format(),
        })
      ).format(),
      nbDataSent: faker.number.int({ min: 0, max: 10000 }),
      counter: 0,

      //   IoT Device data
      ...ioTDeviceInstantaneousData[iotDevice.deviceType],
    });

  // State to simulate device default
  const [fault, setFault] = React.useState({
    state: false,
    duration: faker.number.int({ min: 60000, max: 120000 }),
  });

  //   Get iot device data measurement name;
  const instantaneousDataKeys = React.useMemo(
    () => Object.keys(ioTDeviceInstantaneousData[iotDevice.deviceType]),
    [iotDevice]
  );

  // Topic to publish the iot device passed in parameter as props
  const pubTopic = React.useMemo(
    () => `${IoTDevicePrefix[iotDevice.deviceType]}-${iotDevice.deviceID}`,
    [iotDevice]
  );

  // client id on mqtt broker of this component
  const clientId = React.useMemo(
    () => `${IoTDevicePrefix[iotDevice.deviceType]}-${iotDevice.deviceID}`,
    [iotDevice]
  );

  // Set React Query mutation to save a new IoT Device data
  const { mutateAsync: saveDataMutation } = useMutation({
    mutationFn: async (iotDeviceData: TIotDevicesData) => {
      return await saveData(iotDeviceData, iotDevice);
    },
    onSuccess: (data) => {
      // console.log(
      //   "=========== Save data into the database successfully ==========="
      // );

      setInstantaneousData((instantaneousData) => ({
        ...instantaneousData,
        ...ioTDeviceInstantaneousData[iotDevice.deviceType],
        nbDataSent: instantaneousData.nbDataSent + 1,
        counter: 0,
      }));
    },
    onError: (err: any) => {
      console.log("Error: ", err);
    },
  });

  // Initialize Mqtt over websocket connection on MQTT broker for this component
  React.useEffect(() => {
    mqttWsClient = mqtt.connect(EnvConfig.WEBSOCKET_URL!, {
      clientId,
      username: EnvConfig.MQTT_WS_USERNAME,
      password: EnvConfig.MQTT_WS_PASSWORD,
      reconnectPeriod: 0,
    });

    mqttWsClient
      .on("connect", () => {
        // console.log(`${clientId} - CONNECTION SUCCESS`);
        mqttWsClient.publish(pubTopic, `${clientId} - CONNECTION SUCCESS`);
        // mqttMessObject.message = JSON.stringify(instantaneousData);
        // mqttWsClient.publish(pubTopic, JSON.stringify(mqttMessObject));
      })
      .on("offline", () => {
        // console.log(`======= onOffline ========= ${clientId}`);
      })
      .on("reconnect", () => {
        // console.log(`======= onReconnect ========= ${clientId}`);
      })
      .on("end", () => {
        // console.log(`======= onEnd ========= ${clientId}`);
      })
      .on("error", (err: any) => {
        console.log(`========== OnError Event ========== ${clientId}`);
        console.log(`Connection to ${EnvConfig.WEBSOCKET_URL} failed`);
        console.log("========== End OnError Event ========== ");
      })
      .on("close", () => {
        // console.log(`========== OnClose Event ========== ${clientId}`);
        console.log(`${clientId} Connection close`);
        // console.log("========== End OnClose Event ========== ");
        // mqttWsClient.reconnect();
      })
      .on("disconnect", (packet: mqtt.IPacket) => {
        // console.log(`========== OnDisconnect Event ========== ${clientId}`);
        // console.log("packet :", packet);
        // console.log("========== End OnDisconnect Event ========== ");
      });

    // Check the connection status on the MQTT broker of the component every 2 seconds and reconnect if disconnected
    const intervalID = setInterval(() => {
      // Automatically reconnect on the MQTT Broker if iot device has no fault
      if (!fault.state) {
        if (mqttWsClient && !mqttWsClient.connected) mqttWsClient.reconnect();
      }
    }, 5000);

    // Clean up function
    return () => {
      clearInterval(intervalID); // Clean up timer interval
    };
  }, []);

  // Generate fake data every 3 seconds and send it to the user interface and store the average into the database every 1 minute
  React.useEffect(() => {
    const intervalID = setInterval(() => {
      setFault((fault) => {
        if (!fault.state) {
          setInstantaneousData((instantaneousData) => {
            const fakeData = { ...instantaneousData };
            const accData = { ...instantaneousData };

            for (const param of instantaneousDataKeys) {
              fakeData[param] = faker.number.float({
                min: iotDeviceFakeDataConfig[iotDevice.deviceType as string][
                  param
                ][0],
                max: iotDeviceFakeDataConfig[iotDevice.deviceType as string][
                  param
                ][1],
                precision: 0.1,
              });
              accData[param] += fakeData[param];
            }
            accData.counter += 1;

            //   Send data to backend and store it into the database
            if (accData.counter >= 20) {
              //   setDataToSave((data) => {
              const _data = { ...iotDeviceData[iotDevice.deviceType] };
              for (const param of instantaneousDataKeys) {
                _data[param] = accData[param] / accData.counter;
              }
              _data.dateTime = moment().format();
              _data.iotDevice = iotDevice.deviceIri;

              saveDataMutation(_data);
            }

            //   Send data to the user interface via mqtt broker
            if (mqttWsClient.connected) {
              mqttMessObject.from = clientId;
              mqttMessObject.message = JSON.stringify(fakeData);
              mqttWsClient.publish(pubTopic, JSON.stringify(mqttMessObject));
            }

            return { ...accData };
          });
        }

        return {
          ...fault,
        };
      });
    }, 3000);

    return () => {
      clearInterval(intervalID); // Clean up timer interval
    };
  }, []);

  // Random fault generation
  React.useEffect(() => {
    const faultIntervalID = setInterval(() => {
      const state = !!faker.number.int({ min: 0, max: 1 });
      if (!state) {
        if (mqttWsClient && !mqttWsClient.connected) mqttWsClient.reconnect();
      } else {
        mqttWsClient.end();
        console.log(`Connection status: ${mqttWsClient.connected}`);
      }

      setFault((fault) => ({
        ...fault,
        state,
      }));
    }, fault.duration);

    return () => {
      window.clearInterval(faultIntervalID);
    };
  }, []);

  return <></>;
};

export default IoTDeviceSimulator;
