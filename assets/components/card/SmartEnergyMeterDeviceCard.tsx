import moment from "moment";
import React from "react";
import { TDeviceCard } from "../../types/DeviceCardType";
import * as IoTDeviceAPI from "../../services/api/iotDevicesAPI";
import mqtt, { MqttClient } from "mqtt";
import { TMqttWsMessage } from "../../types/MqttWsType";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import EnvConfig from "../../config/env.config";
import { toast } from "react-toastify";
import { ApexOptions } from "apexcharts";
import DeviceCard from "./DeviceCard";
import Chart from "react-apexcharts";
import { IoTDevicePrefix } from "../../types/IoTDeviceType";

type SmartEnergyMeterDeviceCardProps = {} & TDeviceCard;

// const nowDate = moment().format("YYYY-MM-DD");

const _xaxis: ApexXAxis = {
  type: "category",
  labels: {
    datetimeFormatter: {
      year: "yyyy",
      month: "MMM 'yy",
      day: "dd MMM",
      hour: "HH:mm",
    },
    show: false,
    formatter: function (val, timestamp) {
      // console.log(val + ":00:00");
      // console.log(new Date(val + ":00:00"));
      return moment(new Date(val)).format("DD MMM YYYY HH:mm");
    },
  },
};

const tooltip: ApexTooltip = {
  theme: "dark",
  y: [
    {
      formatter: function (y) {
        if (typeof y !== "undefined") {
          return " : " + y + " kW";
        }
        return y;
      },
    },
    {
      formatter: function (y) {
        if (typeof y !== "undefined") {
          return " : " + y + " kWh";
        }
        return y;
      },
    },
    {
      formatter: function (y) {
        if (typeof y !== "undefined") {
          return " : " + y + " V";
        }
        return y;
      },
    },
  ],
};

const seriesNames = {
  activePower: "Active Power",
  activeEnergy: "Active Energy",
  volt: "Voltage",
};

let mqttWsClient: MqttClient;

type TSmartEnergyMeterData = {
  activePower: number | string | null;
  activeEnergy: number | string | null;
  voltage: number | string | null;
  workingTime: number | string | null;
  nbDataSent: number | string | null;
  lastUpdated?: string;
  connectionStatus?: boolean;
  nbDisconnection?: number;
  disconnectionDate?: string;
};

const SmartEnergyMeterDeviceCard: React.FC<SmartEnergyMeterDeviceCardProps> = ({
  iotDevice,
}) => {
  const dateRange: IoTDeviceAPI.TDateRange = {
    startDate: moment().format("YYYY-MM-DD"),
    endDate: moment().format("YYYY-MM-DD"),
  };

  const [realTimeData, setRealTimeData] = React.useState<TSmartEnergyMeterData>(
    {
      activePower: null,
      activeEnergy: null,
      voltage: null,
      workingTime: null,
      nbDataSent: null,
      lastUpdated: "",
      connectionStatus: false,
      nbDisconnection: 0,
      disconnectionDate: "",
    }
  );

  // Topic to subscribe to in order to intercept messages from the climate iot device passed in parameter as props
  const subsTopic = React.useMemo(
    () => `${IoTDevicePrefix[iotDevice.deviceType!]}-${iotDevice.deviceID}`,
    [iotDevice]
  );

  const queryClient = useQueryClient();

  // client id on mqtt broker of this component
  const clientId = React.useMemo(
    () => `device-${iotDevice.deviceID}-monitor`,
    [iotDevice]
  );

  // Initialize Mqtt over websocket connection on MQTT broker for this component
  React.useEffect(() => {
    mqttWsClient = queryClient.getQueryData([EnvConfig.MQTT_WS_CLIENT_QKEY])!;

    mqttWsClient.on("message", (topic: string, message: Buffer) => {
      // console.log(`"========== OnMessage Event ========== ${clientId}`);

      if (topic === subsTopic) {
        const strMess = new TextDecoder("utf-8").decode(message);

        try {
          const packet = JSON.parse(strMess) as TMqttWsMessage;
          if (packet.to === "Monitor" && packet.subject === "Data") {
            const data = JSON.parse(packet.message) as TSmartEnergyMeterData;
            setRealTimeData({
              ...data,
              nbDisconnection: 0,
              lastUpdated: String(new Date()),
              connectionStatus: true,
            });
          }
        } catch (error) {}
      }
      // console.log("========== End OnMessage Event ========== ");
    });
  }, []);

  // Management of the disconnection notifications of the iot device passed in parameter as props
  React.useEffect(() => {
    const intervalID = setInterval(() => {
      const mqttConnectedDevices: string[] =
        queryClient.getQueryData([EnvConfig.CONNECTED_DEVICES_QKEY]) || [];

      const isConnected = mqttConnectedDevices.includes(
        `device-${iotDevice.deviceID}-monitor`
      );

      setRealTimeData((realTimeData) => {
        let nbDisconnection = realTimeData.nbDisconnection! + 1;
        let disconnectionDate = realTimeData.disconnectionDate;

        if (!isConnected) {
          // Set the disconnection date
          if (realTimeData.nbDisconnection === 0)
            disconnectionDate = moment(new Date()).format(
              "DD MMM YYYY HH:mm:ss"
            );

          // Condition to avoid infinite notifications
          if (nbDisconnection === 3) {
            // Display the notification of disconnection of the iot device passed in parameter as props
            toast.error(
              <>
                <div>
                  Smart Energy Meter IoT Device ({iotDevice.deviceID}) is
                  permanently disconnected
                </div>
                <small className="text-body-primary text-end">
                  {realTimeData.disconnectionDate!}
                </small>
              </>,
              {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
              }
            );
          }
        } else nbDisconnection = 0;

        return {
          ...realTimeData,
          connectionStatus: nbDisconnection >= 3 ? false : true,
          nbDisconnection,
          disconnectionDate,
        };
      });
    }, 10000);

    return () => {
      clearInterval(intervalID);
    };
  }, []);

  //   Set react Query to GET IoT Devices Data Collection
  const queryKey = ["IoTDevices." + iotDevice.devicePrimaryKey! + "-Data"];

  // Configuration of the request to fetch data stored in the database of the iot device passed in parameter as props
  const { data, isLoading } = useQuery({
    queryKey: queryKey,
    refetchOnReconnect: true,
    refetchOnWindowFocus: false,
    refetchInterval: 10000,
    refetchIntervalInBackground: true,
    refetchOnMount: true,
    // enabled: false,
    queryFn: async () => {
      return await IoTDeviceAPI.getData(iotDevice.devicePrimaryKey!, dateRange);
    },
    onSuccess: (data_: any) => {
      // console.log(data_);
    },
    onError: (error: any) => {
      console.log(error);
    },
  });

  // Setting ApexCharts series
  const series = React.useMemo(
    () => [
      {
        name: seriesNames.activePower, //will be displayed on the y-axis
        data: data && data.activePower ? data.activePower : [],
      },
      {
        name: seriesNames.activeEnergy, //will be displayed on the y-axis
        data: data && data.activeEnergy ? data.activeEnergy : [],
      },
      {
        name: seriesNames.volt, //will be displayed on the y-axis
        data: data && data.voltage ? data.voltage : [],
      },
    ],
    [data]
  );

  // ApexCharts options configuration
  const options: ApexOptions = React.useMemo(
    () => ({
      chart: {
        id: "power-energy-voltage",
        toolbar: {
          show: false,
        },
        stroke: {
          curve: "smooth",
        },
      },
      noData: {
        text: isLoading ? "Loading..." : "No Data to display",
      },
      xaxis: {
        categories: data && data.date ? data.date : [], //will be displayed on the x-asis
        ..._xaxis,
      },
      yaxis: [
        {
          seriesName: seriesNames.activeEnergy,
          opposite: false,
          title: {
            text: "Active Energy (kWh)",
          },
        },
        {
          seriesName: seriesNames.activePower,
          opposite: true,
          title: {
            text: "Active Power (kW)",
          },
        },
        {
          seriesName: seriesNames.volt,
          opposite: true,
          title: {
            text: "Voltage (V)",
          },
        },
      ],
      tooltip,
    }),
    [data]
  );

  // Fragment for displaying the current data of the iot device passed in parameter as props
  const element = React.useMemo(
    () => (
      <>
        <div className="d-flex justify-content-start">
          <div className="d-flex flex-column">
            {/* Active Power Data section */}
            <div className="d-flex flex-row gap-2 align-items-center h5 card-title ">
              <span className="">Active Power : </span>
              <span>
                {realTimeData.activePower !== null
                  ? `${realTimeData.activePower} kW`
                  : "-"}
              </span>
            </div>

            {/* Active Energy Data section */}
            <div className="d-flex flex-row gap-2 align-items-center h5 card-title ">
              <span className="">Active Energy : </span>
              <span>
                {realTimeData.activeEnergy !== null
                  ? `${realTimeData.activeEnergy} kWh`
                  : "-"}
              </span>
            </div>

            {/* Voltage Data section */}
            <div className="d-flex flex-row gap-2 align-items-center h5 card-title ">
              <span className="">Voltage : </span>
              <span>
                {realTimeData.voltage !== null
                  ? `${realTimeData.voltage} V`
                  : "-"}
              </span>
            </div>

            {/* Working Time Data section */}
            <div className="d-flex flex-row gap-2 align-items-center h5 card-title ">
              <span className="">Working Time : </span>
              <span>
                {realTimeData.workingTime !== null
                  ? `Since ${moment(
                      new Date(realTimeData.workingTime)
                    ).calendar()}`
                  : "-"}
              </span>{" "}
            </div>

            {/* Number of Data Sent section */}
            <div className="d-flex flex-row gap-2 align-items-center h5 card-title ">
              <span className="">Number of data sent : </span>
              <span>
                {realTimeData.nbDataSent !== null
                  ? `${realTimeData.nbDataSent}`
                  : "-"}
              </span>
            </div>
          </div>
        </div>
      </>
    ),
    [realTimeData]
  );

  return (
    <>
      <DeviceCard
        title={`${iotDevice.deviceName!} (#${iotDevice.deviceID})`}
        isConnected={realTimeData.connectionStatus!}
        chart={
          <Chart options={options} type="line" series={series} width="100%" />
        }
        lastUpdateTime={realTimeData.lastUpdated}
        realTimeData={element}
      />
      {/* <div className="card h-100">
        <div className="card-header">{iotDevice.deviceName}</div>
        <img
          src="https://placehold.co/600x400"
          className="card-img-top rounded-top-0"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title">Card title SMART ENERGY METER</h5>
          <p className="card-text">
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This content is a little bit longer.
          </p>
        </div>
        <div className="card-footer">
          <small className="text-body-secondary">Last updated 3 mins ago</small>
        </div>
      </div> */}
    </>
  );
};

export default SmartEnergyMeterDeviceCard;
