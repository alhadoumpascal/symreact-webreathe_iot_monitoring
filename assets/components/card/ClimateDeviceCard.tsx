import { useQuery, useQueryClient } from "@tanstack/react-query";
import { ApexOptions } from "apexcharts";
import moment from "moment";
import mqtt, { MqttClient } from "mqtt";
import React from "react";
import Chart from "react-apexcharts";
import { toast } from "react-toastify";
import EnvConfig from "../../config/env.config";
import * as IoTDeviceAPI from "../../services/api/iotDevicesAPI";
import { TDeviceCard } from "../../types/DeviceCardType";
import { IoTDevicePrefix } from "../../types/IoTDeviceType";
import { TMqttWsMessage } from "../../types/MqttWsType";
import DeviceCard from "./DeviceCard";

type ClimateDeviceCardProps = {} & TDeviceCard;

const _xaxis: ApexXAxis = {
  type: "category",
  labels: {
    datetimeFormatter: {
      year: "yyyy",
      month: "MMM 'yy",
      day: "dd MMM",
      hour: "HH:mm",
    },
    show: false,
    formatter: function (val, timestamp) {
      // console.log(val + ":00:00");
      // console.log(new Date(val + ":00:00"));
      return moment(new Date(val)).format("DD MMM YYYY HH:mm");
    },
  },
};

const tooltip: ApexTooltip = {
  theme: "dark",
  y: [
    {
      formatter: function (y) {
        if (typeof y !== "undefined") {
          return " : " + y + " °C";
        }
        return y;
      },
    },
    {
      formatter: function (y) {
        if (typeof y !== "undefined") {
          return " : " + y + " %";
        }
        return y;
      },
    },
  ],
};

const seriesNames = {
  temperature: "Temperature",
  humidity: "Humidity",
};

let mqttWsClient: MqttClient;

type TClimateData = {
  temperature: number | string | null;
  humidity: number | string | null;
  workingTime: number | string | null;
  nbDataSent: number | string | null;
  lastUpdated?: string;
  connectionStatus?: boolean;
  nbDisconnection?: number;
  disconnectionDate?: string;
};

const ClimateDeviceCard: React.FC<ClimateDeviceCardProps> = ({ iotDevice }) => {
  const dateRange: IoTDeviceAPI.TDateRange = {
    startDate: moment().format("YYYY-MM-DD"),
    endDate: moment().format("YYYY-MM-DD"),
  };

  // Local state to display the current data of the climate type iot device passed in parameter as props
  const [realTimeData, setRealTimeData] = React.useState<TClimateData>({
    temperature: null,
    humidity: null,
    workingTime: null,
    nbDataSent: null,
    lastUpdated: "",
    connectionStatus: false,
    nbDisconnection: 0,
    disconnectionDate: "",
  });

  // Topic to subscribe to in order to intercept messages from the climate iot device passed in parameter as props
  const subsTopic = React.useMemo(
    () => `${IoTDevicePrefix[iotDevice.deviceType!]}-${iotDevice.deviceID}`,
    [iotDevice]
  );

  const queryClient = useQueryClient();

  // client id on mqtt broker of this component
  const clientId = React.useMemo(
    () => `device-${iotDevice.deviceID}-monitor`,
    [iotDevice]
  );

  // Initialize Mqtt over websocket connection on MQTT broker for this component
  React.useEffect(() => {
    mqttWsClient = queryClient.getQueryData([EnvConfig.MQTT_WS_CLIENT_QKEY])!;

    mqttWsClient.on(
      "message",
      (topic: string, message: Buffer, packet: mqtt.IPublishPacket) => {
        // console.log(`"========== OnMessage Event ========== ${clientId}`);

        if (topic === subsTopic) {
          const strMess = new TextDecoder("utf-8").decode(message);

          try {
            const packet = JSON.parse(strMess) as TMqttWsMessage;
            if (packet.to === "Monitor" && packet.subject === "Data") {
              const data = JSON.parse(packet.message) as TClimateData;

              setRealTimeData({
                ...data,
                nbDisconnection: 0,
                lastUpdated: String(new Date()),
                connectionStatus: true,
              });
            } else {
              setRealTimeData((realTimeData) => ({
                ...realTimeData,
                nbDisconnection: 0,
                connectionStatus: true,
              }));
            }
          } catch (error) {}
        }

        // console.log("========== End OnMessage Event ========== ");
      }
    );
  }, []);

  // Management of the disconnection notifications of the iot device passed in parameter as props
  React.useEffect(() => {
    const intervalID = setInterval(() => {
      const mqttConnectedDevices: string[] =
        queryClient.getQueryData([EnvConfig.CONNECTED_DEVICES_QKEY]) || [];

      const isConnected = mqttConnectedDevices.includes(
        `${IoTDevicePrefix[iotDevice.deviceType!]}-${iotDevice.deviceID}`
      );

      setRealTimeData((realTimeData) => {
        let nbDisconnection = realTimeData.nbDisconnection! + 1;
        let disconnectionDate = realTimeData.disconnectionDate;

        if (!isConnected) {
          // Set the disconnection date
          if (realTimeData.nbDisconnection === 0)
            disconnectionDate = moment(new Date()).format(
              "DD MMM YYYY HH:mm:ss"
            );

          // Condition to avoid infinite notifications
          if (nbDisconnection === 3) {
            // Display the notification of disconnection of the iot device passed in parameter as props
            toast.error(
              <>
                <div>
                  Climate IoT Device ({iotDevice.deviceID}) is permanently
                  disconnected
                </div>
                <small className="text-body-primary text-end">
                  {realTimeData.disconnectionDate!}
                </small>
              </>,
              {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
              }
            );
          }
        } else nbDisconnection = 0;

        return {
          ...realTimeData,
          connectionStatus: nbDisconnection >= 3 ? false : true,
          nbDisconnection,
          disconnectionDate,
        };
      });
    }, 10000);

    return () => {
      clearInterval(intervalID);
    };
  }, []);

  //   Set react Query to GET IoT Devices Data Collection
  const queryKey = ["IoTDevices." + iotDevice.devicePrimaryKey! + "-Data"];

  // Configuration of the request to fetch data stored in the database of the iot device passed in parameter as props
  const { data, isLoading } = useQuery({
    queryKey: queryKey,
    refetchOnReconnect: true,
    refetchOnWindowFocus: false,
    refetchInterval: 10000,
    refetchIntervalInBackground: true,
    refetchOnMount: true,
    // enabled: false,
    queryFn: async () => {
      return await IoTDeviceAPI.getData(iotDevice.devicePrimaryKey!, dateRange);
    },
    onSuccess: (data_: any) => {
      // console.log(data_);
    },
    onError: (error: any) => {
      console.log(error);
    },
  });

  // Setting ApexCharts series
  const series = React.useMemo(
    () => [
      {
        name: seriesNames.temperature, //will be displayed on the y-axis
        data: data && data.temperature ? data.temperature : [], //[43, 53, 50, 57]
      },
      {
        name: seriesNames.humidity, //will be displayed on the y-axis
        data: data && data.humidity ? data.humidity : [], //[88, 69, 95, 77]
      },
    ],
    [data]
  );

  // ApexCharts options configuration
  const options: ApexOptions = React.useMemo(
    () => ({
      chart: {
        id: "temperature-humidity",
        toolbar: {
          show: false,
        },
        stroke: {
          curve: "smooth",
        },
      },
      noData: {
        text: isLoading ? "Loading..." : "No Data to display",
      },
      xaxis: {
        categories: data && data.date ? data.date : [], //will be displayed on the x-asis
        ..._xaxis,
      },
      yaxis: [
        {
          seriesName: seriesNames.temperature,
          opposite: false,
          title: {
            text: "Temperature (°C)",
          },
        },
        {
          seriesName: seriesNames.humidity,
          opposite: true,
          title: {
            text: "Humidity (%)",
          },
        },
      ],
      tooltip,
    }),
    [data]
  );

  // Fragment for displaying the current data of the iot device passed in parameter as props
  const element = React.useMemo(
    () => (
      <>
        <div className="d-flex justify-content-start">
          <div className="d-flex flex-column">
            {/* Temperature Data section */}
            <div className="d-flex flex-row gap-2 align-items-center h5 card-title ">
              <small className="">Temperature : </small>
              <small>
                {realTimeData.temperature !== null
                  ? `${realTimeData.temperature} °C`
                  : "-"}
              </small>
            </div>

            {/* Humidity Data section */}
            <div className="d-flex flex-row gap-2 align-items-center h5 card-title ">
              <small className="">Humidity : </small>
              <small>
                {realTimeData.humidity !== null
                  ? `${realTimeData.humidity} %`
                  : "-"}
              </small>
            </div>

            {/* Working Time Data section */}
            <div className="d-flex flex-row gap-2 align-items-center h5 card-title ">
              <small className="">Working Time : </small>
              <small>
                {realTimeData.workingTime !== null
                  ? `Since ${moment(
                      new Date(realTimeData.workingTime)
                    ).calendar()}`
                  : "-"}
              </small>{" "}
            </div>

            {/* Number of Data Sent section */}
            <div className="d-flex flex-row gap-2 align-items-center h5 card-title ">
              <small className="">Number of data sent : </small>
              <small>
                {realTimeData.nbDataSent !== null
                  ? realTimeData.nbDataSent
                  : "-"}
              </small>
            </div>
          </div>
        </div>
      </>
    ),
    [realTimeData]
  );

  return (
    <>
      <DeviceCard
        title={`${iotDevice.deviceName!} (#${iotDevice.deviceID})`}
        isConnected={realTimeData.connectionStatus!}
        chart={
          <Chart options={options} type="line" series={series} width="100%" />
        }
        lastUpdateTime={realTimeData.lastUpdated}
        realTimeData={element}
      />
    </>
  );
};

export default ClimateDeviceCard;
