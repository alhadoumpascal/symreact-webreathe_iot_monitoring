import React from "react";
import { GoPrimitiveDot } from "react-icons/go";
import moment from "moment";

type DeviceCardProps = {
  title: string; // IoT Device name
  isConnected: boolean; // Device mqtt connection status
  chart?: JSX.Element; // ApexChart to display iot device data measured
  realTimeData?: JSX.Element; // React Element to display iot device's real time data measured
  lastUpdateTime?: string;
};

const DeviceCard: React.FC<DeviceCardProps> = ({
  title,
  isConnected,
  chart,
  realTimeData,
  lastUpdateTime,
}) => {
  return (
    <>
      <div className="card h-100" style={{ width: "90%" }}>
        <div className="card-header d-flex justify-content-between">
          <span className="">{title}</span>
          <span
            className={`text-${
              isConnected ? "success" : "danger"
            } align-items-center d-flex`}
          >
            <GoPrimitiveDot fontSize={"20px"} />
            {isConnected ? "Connected" : "Disconnected"}
          </span>
        </div>

        <div className="card-body">
          {/* <h5 className="card-title">Card title CLIMATE</h5> */}
          {realTimeData && realTimeData}
        </div>
        {chart && chart}
        {lastUpdateTime && (
          <div className="card-footer">
            <small className="text-body-secondary">
              Last updated {moment(new Date(lastUpdateTime)).fromNow()}
            </small>
          </div>
        )}
      </div>
    </>
  );
};

export default DeviceCard;
