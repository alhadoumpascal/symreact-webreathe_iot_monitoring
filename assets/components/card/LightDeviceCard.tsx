import React from "react";
import { TDeviceCard } from "../../types/DeviceCardType";

type LightDeviceCardProps = {} & TDeviceCard;

const LightDeviceCard: React.FC<LightDeviceCardProps> = ({ iotDevice }) => {
  return (
    <>
      <div className="card h-100">
        <div className="card-header">{iotDevice.deviceName}</div>
        <div className="card-body">
          <h5 className="card-title">Card title LIGHT</h5>
          <p className="card-text">
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This content is a little bit longer.
          </p>
        </div>
        <img
          src="https://placehold.co/600x400"
          className="card-img-top"
          alt="..."
        />
        <div className="card-footer">
          <small className="text-body-secondary">Last updated 3 mins ago</small>
        </div>
      </div>
    </>
  );
};

export default LightDeviceCard;
