import React from "react";
import CustomButton from "./form/CustomButton";

type Props = {
  children: React.ReactNode;
  title: string;
  actionBtnClick: () => void;
  [x: string]: any;
  actionBtnText?: string;
  cancelBtnText?: string;
  cancelBtnClick?: () => void;
  isLoading?: boolean;
  isSubmitting?: boolean;
  hasCancelBtn?: boolean;
  actionBtn?: JSX.Element;
};

const CustomAppModal = React.forwardRef(
  (
    {
      title,
      actionBtnText,
      cancelBtnText,
      actionBtnClick,
      cancelBtnClick,
      children,
      isLoading,
      isSubmitting,
      hasCancelBtn = false,
      actionBtn,
      ...rest
    }: Props,
    ref
  ) => {
    
    const [isOpen, setIsOpen] = React.useState(false);

    const openModal = React.useCallback(
      () => {
        console.log("open modal trigged !");
        setIsOpen(true);
      },
      [],
    );
    const closeModal = React.useCallback(
      () => {
        setIsOpen(false);
      },
      [],
    );

    React.useImperativeHandle(ref, () => ({
      openModal,
      closeModal,
    }));

    return (
      <>
        
        <div
          className={`modal fade ${isOpen ? "show" : ""}`}
          tabIndex={-1}
          role="dialog"
          aria-hidden={isOpen ? "false" : "true"}
          aria-modal={isOpen ? "true" : "false"}
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">{title}</h5>
                {!isSubmitting && (
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true"></span>
                  </button>
                )}
              </div>
              <div className="modal-body">{children}</div>
              <div className="modal-footer">
                {actionBtn || (
                  <CustomButton
                    loading={isLoading}
                    onClick={actionBtnClick}
                    className="me-2"
                  >
                    {actionBtnText || "Save"}
                  </CustomButton>
                )}
                {hasCancelBtn && (
                  <CustomButton
                    onClick={cancelBtnClick || closeModal}
                    variant="secondary"
                    className="me-3"
                  >
                    {cancelBtnText || "Close"}
                  </CustomButton>
                )}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
);

export default CustomAppModal;
