import { useQuery } from "@tanstack/react-query";
import React from "react";
import { toast } from "react-toastify";
import ClimateDeviceCard from "../components/card/ClimateDeviceCard";
import SmartEnergyMeterDeviceCard from "../components/card/SmartEnergyMeterDeviceCard";
import DashboardLoader from "../components/loader/DashboardLoader";
import { getAll } from "../services/api/deviceCategoryAPI";

import { IDeviceCategoryType } from "../types/DeviceCategoryType";

const DashboardPage = () => {
  // Initialize the react Query service to get all iot device's categories from server
  const deviceCategoriesQueryKey = ["DeviceCategories"];
  const { data: deviceCategories, isLoading: isLoadingDeviceCategories } =
    useQuery({
      queryKey: deviceCategoriesQueryKey,
      queryFn: async () => await getAll(),
      refetchOnReconnect: true,
      refetchOnWindowFocus: false,
      refetchInterval: false,
      refetchIntervalInBackground: true,
      refetchOnMount: true,
      onSuccess: (_deviceCategories: IDeviceCategoryType[]) => {
        // console.log("onSuccess")
        console.log(_deviceCategories);
      },
      onError: (err) => {
        toast.error(
          "An error occurred while retrieving the list of IoT Device Categories."
        );
      },
    });

  const accordionRefs = React.useRef<HTMLDivElement[]>([]);

  const addAccordionRef = (ref: HTMLDivElement) => {
    if (ref && !accordionRefs.current.includes(ref))
      accordionRefs.current.push(ref);
  };

  /**
   * Function which allows to Toggle the Accordion's Collapse State
   *
   * @param e Accordion Button Event which triggered
   * @param index
   */
  const handleAccordionBtn = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    index: number
  ) => {
    // Accordion button which triggered click event
    const btnEl = e.currentTarget.classList;

    // Accordion to toggle
    const divEl = accordionRefs.current[index];

    // Toggle class .show
    if (divEl.classList.contains("show")) {
      divEl.classList.remove("show");

      // Toggle class .collapsed
      if (!btnEl.contains("collapsed")) btnEl.add("collapsed");
    } else {
      divEl.classList.add("show");

      // Toggle class .collapsed
      if (btnEl.contains("collapsed")) btnEl.remove("collapsed");
    }
  };

  // const deviceCategories = isLoadingDeviceCategories ? [] : data;

  return (
    <>
      {isLoadingDeviceCategories ? (
        <DashboardLoader />
      ) : (
        <div className="accordion">
          {deviceCategories?.map((category, index) => {
            return (
              <div
                className={`accordion-item ${
                  index !== 0 && "my-5 border-top rounded-top"
                }`}
                key={category.deviceCategoryName}
              >
                <h2 className="accordion-header">
                  <button
                    className="accordion-button"
                    type="button"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                    onClick={(e) => {
                      handleAccordionBtn(e, index);
                    }}
                  >
                    {category.deviceCategoryName}
                  </button>
                </h2>
                <div
                  className="accordion-collapse collapse show"
                  ref={addAccordionRef}
                >
                  <div className="accordion-body">
                    <div className="row row-cols-1 row-cols-md-2 g-4">
                      {category.ioTDevices.map((device, index) => (
                        <div className="col" key={device.deviceID}>
                          {/* {category.deviceCategoryName === "LIGHT" && (
                            <LightDeviceCard iotDevice={device} />
                          )} */}
                          {category.deviceCategoryName === "CLIMATE" && (
                            <ClimateDeviceCard iotDevice={device} />
                          )}
                          {category.deviceCategoryName ===
                            "SMART ENERGY METER" && (
                            <SmartEnergyMeterDeviceCard iotDevice={device} />
                          )}
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default DashboardPage;
