import React from 'react'
import IoTDevicesDataTableHandler from '../../containers/dataTableHandlers/IoTDevicesDataTableHandler'

const HomePage = () => {
  return (
    <>
      <IoTDevicesDataTableHandler /> 
    </>
  )
}

export default HomePage