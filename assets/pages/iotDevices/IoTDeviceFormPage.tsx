import React from 'react'
import IoTDeviceFormHandler from '../../containers/formHandlers/IoTDeviceFormHandler'

const IoTDeviceFormPage = () => {
  return (
    <IoTDeviceFormHandler />
  )
}

export default IoTDeviceFormPage