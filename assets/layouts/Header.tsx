import React from "react";
import { NavLink } from "react-router-dom";
import URLConfig from "../config/urls/url.config";

const headerLink = [
  {
    path: URLConfig.DASHBOARD,
    text: "Dashboard",
  },
  {
    path: URLConfig.IOT_DEVICE.ROOT,
    text: "IoT Devices",
  },
];

const Header = () => {
  const NavBarRef = React.useRef<HTMLDivElement>(null);

  /**
   * Function which allows to Toggle the responsive NavBar
   *
   * @param e Responsive NavBar Button Event which triggered
   *
   */
  const handleNavBarBtn = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    // Accordion button which triggered click event
    const btnEl = e.currentTarget.classList;
    console.log(NavBarRef.current);
    // Accordion to toggle
    const divEl = NavBarRef.current!;

    // Toggle class .show
    if (divEl.classList.contains("show")) {
      divEl.classList.remove("show");

      // Toggle class .collapsed
      if (!btnEl.contains("collapsed")) btnEl.add("collapsed");
    } else {
      divEl.classList.add("show");

      // Toggle class .collapsed
      if (btnEl.contains("collapsed")) btnEl.remove("collapsed");
    }
  };

  return (
    <nav className="navbar navbar-expand-lg bg-primary " data-bs-theme="dark">
      <div className="container-fluid">
        <a
          data-testid="linkElement"
          href="https://www.webreathe.fr"
          target="_blank"
          className="navbar-brand"
        >
          <img
            src="https://static.wixstatic.com/media/9cc29e_e61b56b15fb94021a7401bc61dc4c65d~mv2.png/v1/fill/w_36,h_36,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/Spyralle.png"
            alt="Spyralle.png"
            srcSet="https://static.wixstatic.com/media/9cc29e_e61b56b15fb94021a7401bc61dc4c65d~mv2.png/v1/fill/w_36,h_36,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/Spyralle.png 1x, https://static.wixstatic.com/media/9cc29e_e61b56b15fb94021a7401bc61dc4c65d~mv2.png/v1/fill/w_72,h_72,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/Spyralle.png 2x"
            width="36"
            height="36"
            className="me-2"
          />
        </a>

        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarColor03"
          aria-controls="navbarColor03"
          aria-expanded="false"
          aria-label="Toggle navigation"
          onClick={(e) => {
            handleNavBarBtn(e);
          }}
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className="collapse navbar-collapse"
          id="navbarColor03"
          ref={NavBarRef}
        >
          <ul className="navbar-nav me-auto">
            {headerLink.map((link, index) => {
              return (
                <li key={index} className="nav-item">
                  <NavLink
                    to={link.path}
                    className={({ isActive }) =>
                      `nav-link ${isActive ? "active " : ""}`
                    }
                  >
                    {link.text}
                  </NavLink>
                </li>
              );
            })}
          </ul>
        </div>

        {/* <div
          className="offcanvas offcanvas-start"
          tabIndex={-1}
          id="offcanvasNavbar"
          aria-labelledby="offcanvasNavbarLabel"
          ref={NavBarRef}
        >
          <div className="offcanvas-header">
            <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
              WeBreathe
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>

          <div className="offcanvas-body">
            <ul className="navbar-nav justify-content-end flex-grow-1 pe-3">
              {headerLink.map((link, index) => {
                return (
                  <li key={index} className="nav-item">
                    <NavLink
                      to={link.path}
                      className={({ isActive }) =>
                        `nav-link ${isActive ? "active" : ""}`
                      }
                    >
                      {link.text}
                    </NavLink>
                  </li>
                );
              })}
            </ul>
          </div>
        </div> */}
      </div>
    </nav>
  );
};

export default Header;
