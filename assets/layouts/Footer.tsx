import React from 'react'
import EnvConfig from '../config/env.config'

const Footer = () => {
  return (
    <footer className={`d-flex justify-content-between justify-md-content-space-between mb-3 px-4 mt-5 fixed-sticky`}>
    <div className='inline'>© 2023 {EnvConfig.APP_NAME}</div> <div className="inline ms-2"> Coded with ❤️ by <a href='https://www.linkedin.com/in/pascal-alhadoum-7454a5a4/' target={"_blank"} className="text-[#6c757d]" style={{color: "#6c757d"}}>Pascal ALHADOUM</a></div>
    </footer>
    // <footer className={`text-center text-lg-start mb-3 px-lg-2 fixed-bottom`}>
    // <span>© 2023 {EnvConfig.APP_NAME}</span> <span className="inline-block w-100 text-center float-lg-auto w-md-auto">Coded with ❤️ by <a href='https://www.linkedin.com/in/pascal-alhadoum-7454a5a4/' target={"_blank"} className="text-[#6c757d]">Pascal ALHADOUM</a></span>
    // </footer>
  )
}

export default Footer