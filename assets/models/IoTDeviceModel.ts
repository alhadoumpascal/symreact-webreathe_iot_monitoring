import { TCategoryName } from "../types/DeviceCategoryType";
import { I_IoTDeviceType } from "../types/IoTDeviceType";

export class IoTDeviceModel implements I_IoTDeviceType {

    constructor(
        public devicePrimaryKey: string | number,
        public deviceID: string,
        public deviceName: string,
        public deviceType: TCategoryName,
        public deviceIri: string,
        public categoryIri: string,
    ){}
}