export function auth (prefix: string) {
    return {
        ROOT: prefix,
        LOGIN: `${prefix}/login`,
        LOGOUT: `${prefix}/logout`,
        
    }
}