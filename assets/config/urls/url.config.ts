import * as APPURLS from './';

export type URLType = { [key: string]: string };

export const ROOT_URL = '/';

export const URL_PREFIX = {
  ROOT_URL,

  AUTH: `${ROOT_URL}auth`,
  
  // IoT Device
  IOT_DEVICE: `${ROOT_URL}iot-devices`, 
};

// ALL APP ROUTES
const URLConfig = {
  ROOT: ROOT_URL,
  NOT_FOUND: '/not-found',
  DASHBOARD: '/dashboard',

  // IoT Device
  IOT_DEVICE: {
    ...APPURLS.iotDevice(URL_PREFIX.IOT_DEVICE),
  },

  // Auth Module
  AUTH: {
    ...APPURLS.auth(URL_PREFIX.AUTH),
  },

};

console.log(URLConfig);

export default URLConfig;
