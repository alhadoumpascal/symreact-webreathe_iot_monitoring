const EnvConfig = {
  APP_NAME: process.env.REACT_APP_NAME,
  API_URL: process.env.API_URL,
  
  // MQTT Broker configuration
  BROKER_HOST: process.env.BROKER_HOST,
  BROKER_MANAGEMENT_PORT: process.env.BROKER_MANAGEMENT_PORT,

  MQTT_WS_USERNAME: process.env.MQTT_WS_USERNAME,
  MQTT_WS_PASSWORD: process.env.MQTT_WS_PASSWORD,
  MQTT_WS_PORT: process.env.MQTT_WS_PORT,

  WEBSOCKET_URL: `ws://${process.env.BROKER_HOST}:${process.env.MQTT_WS_PORT}/ws`,

  // React Query Keys
  CONNECTED_DEVICES_QKEY: "Connected-IoTDevices",
  MQTT_WS_CLIENT_QKEY: "MqttWsClient",
  IOT_DEVICES_QKEY: "IotDevices",
  DEVICE_CATGO_QKEY: "DeviceCategories",
};

console.log(EnvConfig);

export default EnvConfig;
