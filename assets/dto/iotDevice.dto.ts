import { TMeta } from "../services/api/api";
import { TDeviceCategoryDto } from "./deviceCategory.dto";

export type TIoTDeviceDto = {
    id?: string | number;
    name: string;
    deviceId: string;
    deviceCategory: TDeviceCategoryDto | string;
    createdAt?: string;
} & Partial<TMeta>;

export type TClimateDeviceDataDto = {
    id?: string | number;
    temperature: number;
    humidity: number;
    dateTime: string;
    iotDevice: string;
} & Partial<TMeta>;

export type TSmartEnergyDeviceDataDto = {
    id?: string | number;
    voltage: number;
    activePower: number;
    activeEnergy: number;
    dateTime: string;
    iotDevice: string;
} & Partial<TMeta>;