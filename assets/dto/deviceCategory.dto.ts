import { TMeta } from "../services/api/api";
import { TIoTDeviceDto } from "./iotDevice.dto";

export type TDeviceCategoryDto = {
    id?: string | number;
    name: string;
    ioTDevices?: TIoTDeviceDto[] | [];
} & Partial<TMeta>