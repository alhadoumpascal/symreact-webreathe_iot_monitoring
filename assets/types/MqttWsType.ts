export type TMqttWsOptions = {
  clientId: string;
  topic: string;
  setConnectionStatus: React.Dispatch<React.SetStateAction<boolean>>;
};

export type TMqttWsMessage = {
  from: "Devices" | "Monitor" | string;
  to: "Devices" | "Monitor";
  subject: "Data";
  message: string;
};
