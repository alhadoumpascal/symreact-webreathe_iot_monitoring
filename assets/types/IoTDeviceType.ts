import { TCategoryName } from "./DeviceCategoryType";

export interface I_IoTDeviceType {
  devicePrimaryKey: string | number;
  deviceID: string;
  deviceName: string;
  deviceType: TCategoryName;
  deviceIri: string;
  categoryIri: string;
  createdAt?: string;
};

export enum IoTDevicePrefix {
  CLIMATE = "climate-iot-device",
  "SMART ENERGY METER" = "smart-energy-meter-iot-device",
  "LIGHT" = "light-iot-device",
}