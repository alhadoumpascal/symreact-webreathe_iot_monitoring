import { Schema } from 'yup';

export type FormInputMeta = {
  type?: string,
  label?: string,
  placeholder?: string,
  value?: string,
  name: string,
  checked?: boolean,
  onChange?: (event: any) => void,
  useFormOptions?: any,
  options?: FormSelectInputOptionMeta[],
  validation?: Schema,
  disabled?: boolean,
};

export type FormSelectInputOptionMeta = {
  label: string,
  value: string,
  selected?: boolean,
};
