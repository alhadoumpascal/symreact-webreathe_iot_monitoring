import { I_IoTDeviceType } from "./IoTDeviceType";

export type TCategoryName = "LIGHT" | "CLIMATE" | "SMART ENERGY METER";

export interface IDeviceCategoryType {
    id: string | number;
    iri: string;
    deviceCategoryName: TCategoryName;
    ioTDevices: I_IoTDeviceType[] | [];
  };
  