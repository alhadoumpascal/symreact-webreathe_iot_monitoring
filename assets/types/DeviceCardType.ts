import { I_IoTDeviceType } from "./IoTDeviceType"

export type TDeviceCard = {
    iotDevice: Partial<I_IoTDeviceType>
}