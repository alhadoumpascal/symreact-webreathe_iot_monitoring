export type BSColor = "primary" | "secondary" | "success" | "danger" | "warning" | "info" | "lighter" | "dark";

export type BSSize = "sm" | "md" | "lg" | "xl";