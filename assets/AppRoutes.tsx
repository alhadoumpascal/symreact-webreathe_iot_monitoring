import { useQuery, useQueryClient } from "@tanstack/react-query";
import moment from "moment";
import mqtt from "mqtt";
import React from "react";
import { HashRouter, Route, Routes } from "react-router-dom";
import IoTDeviceSimulator from "./components/IoTDeviceSimulator";
import EnvConfig from "./config/env.config";
import URLConfig from "./config/urls/url.config";
import AppLayout from "./layouts/AppLayout";
import DashboardPage from "./pages/DashboardPage";
import IoTDeviceFormPage from "./pages/iotDevices/IoTDeviceFormPage";
import IoTDevicesPage from "./pages/iotDevices/IoTDevicesPage";
import { getAll } from "./services/api/deviceCategoryAPI";
import { IDeviceCategoryType } from "./types/DeviceCategoryType";
import getConnectedDevices from "./utils/getAllMqttConnectedDevices";

moment.locale();

const AppRoutes: React.FC = () => {
  // Set react Query to GET all connected IoT Devices on Mqtt server
  const queryKey = [EnvConfig.CONNECTED_DEVICES_QKEY];
  useQuery({
    queryKey: queryKey,
    refetchOnReconnect: true,
    refetchOnWindowFocus: false,
    refetchInterval: 3000,
    refetchIntervalInBackground: true,
    refetchOnMount: true,
    queryFn: async () => {
      return await getConnectedDevices();
    },
    onSuccess: (data_: any) => {
      // console.log("========== Connected Devices ===========");
      // console.log(data_);
      // console.log("========== End Connected Devices ===========");
    },
    onError: (error: any) => {
      console.log(error);
    },
  });

  // client id on mqtt broker of this component
  const clientId = React.useMemo(
    () => `iot-device-monitor-${parseInt(String(Math.random() * 100), 10)}`,
    []
  );

  const queryClient = useQueryClient();

  // Initialize Mqtt over websocket connection on MQTT broker for this component
  React.useEffect(() => {
    const mqttWsClient = mqtt.connect(EnvConfig.WEBSOCKET_URL!, {
      // connectTimeout: 30000,
      // keepalive: 60,
      clientId,
      username: EnvConfig.MQTT_WS_USERNAME,
      password: EnvConfig.MQTT_WS_PASSWORD,
      reconnectPeriod: 0,
    });

    mqttWsClient
      .on("connect", () => {
        console.log(`${clientId} - CONNECTION SUCCESS`);
        // setMqttWsConnectionStatus(true);

        mqttWsClient.subscribe("#", { qos: 1 }, (err: any, granted: any) => {
          console.log(`========== OnSubscribe Event ========== ${clientId}`);
          if (err) {
            console.log("Subscription request failed");
          } else console.log(granted);
          console.log("========== End OnSubscribe Event ========== ");
        });
      })
      .on("offline", () => {
        // setMqttWsConnectionStatus(false);
        console.log(`======= onOffline ========= ${clientId}`);
      })
      .on("reconnect", () => {
        // setMqttWsConnectionStatus(false);
        console.log(`======= onReconnect ========= ${clientId}`);
      })
      .on("end", () => {
        // setMqttWsConnectionStatus(false);
        console.log(`======= onEnd ========= ${clientId}`);
      })
      .on("error", (err: any) => {
        console.log(`========== OnError Event ========== ${clientId}`);
        console.log(`Connection to ${EnvConfig.WEBSOCKET_URL} failed`);
        console.log("========== End OnError Event ========== ");
        // setMqttWsConnectionStatus(false);
      })
      .on("close", () => {
        console.log(`========== OnClose Event ========== ${clientId}`);
        console.log("Connection close");
        console.log("========== End OnClose Event ========== ");
      })
      .on("disconnect", (packet: mqtt.IPacket) => {
        console.log(`========== OnDisconnect Event ========== ${clientId}`);
        console.log("packet :", packet);
        console.log("========== End OnDisconnect Event ========== ");
      });

    queryClient.setQueryData(
      [EnvConfig.MQTT_WS_CLIENT_QKEY],
      () => mqttWsClient
    );

    // Check the connection status on the MQTT broker of the component every 5 seconds and reconnect if disconnected
    const intervalID = setInterval(() => {
      if (mqttWsClient && !mqttWsClient.connected) mqttWsClient.reconnect();
    }, 5000);

    // Clean up function
    return () => {
      mqttWsClient.end();
      clearInterval(intervalID); // Clean up timer interval
    };
  }, []);

  // Initialize react query to retrieve all iot device categories and their device, in order to generate a simulator for each piece of iot device

  const { data: deviceCategories, isLoading: isLoadingDeviceCategories } =
    useQuery({
      queryKey: [EnvConfig.DEVICE_CATGO_QKEY],
      queryFn: async () => await getAll(),
      refetchOnReconnect: true,
      refetchOnWindowFocus: false,
      refetchInterval: false,
      refetchIntervalInBackground: true,
      refetchOnMount: true,
      onSuccess: (_deviceCategories: IDeviceCategoryType[]) => {
        // console.log("onSuccess")
        console.log(_deviceCategories);
      },
      onError: (err) => {
        console.log(
          "An error occurred while retrieving the list of IoT Device Categories."
        );
      },
    });

  return (
    <>
      {deviceCategories?.map((category, index) => {
        return category.ioTDevices.map((device, index) => (
          <IoTDeviceSimulator key={device.deviceID} iotDevice={device} />
        ));
      })}
      <HashRouter>
        <Routes>
          <Route path={URLConfig.ROOT} element={<AppLayout />}>
            <Route index element={<IoTDevicesPage />} />
            <Route path={URLConfig.DASHBOARD} element={<DashboardPage />} />
            <Route
              path={URLConfig.IOT_DEVICE.ROOT}
              element={<IoTDevicesPage />}
            />
            <Route
              path={URLConfig.IOT_DEVICE.ROOT + "/:deviceId"}
              element={<IoTDeviceFormPage />}
            />
          </Route>
        </Routes>
      </HashRouter>
    </>
  );
};

export default AppRoutes;
