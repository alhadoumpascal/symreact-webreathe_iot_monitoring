<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230516191724 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE climate_device_data (id INT AUTO_INCREMENT NOT NULL, iot_device_id INT NOT NULL, temperature DOUBLE PRECISION NOT NULL, humidity DOUBLE PRECISION DEFAULT NULL, date_time DATETIME NOT NULL, INDEX IDX_D1C0E17DA89D4A83 (iot_device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE light_device_data (id INT AUTO_INCREMENT NOT NULL, iot_device_id INT NOT NULL, state TINYINT(1) NOT NULL, date_time DATETIME NOT NULL, INDEX IDX_D67CD287A89D4A83 (iot_device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE smart_energy_meter_device_data (id INT AUTO_INCREMENT NOT NULL, iot_device_id INT NOT NULL, voltage DOUBLE PRECISION DEFAULT NULL, active_power DOUBLE PRECISION DEFAULT NULL, active_energy DOUBLE PRECISION DEFAULT NULL, date_time DATETIME NOT NULL, INDEX IDX_92E0204BA89D4A83 (iot_device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE climate_device_data ADD CONSTRAINT FK_D1C0E17DA89D4A83 FOREIGN KEY (iot_device_id) REFERENCES `iot_device` (id)');
        $this->addSql('ALTER TABLE light_device_data ADD CONSTRAINT FK_D67CD287A89D4A83 FOREIGN KEY (iot_device_id) REFERENCES `iot_device` (id)');
        $this->addSql('ALTER TABLE smart_energy_meter_device_data ADD CONSTRAINT FK_92E0204BA89D4A83 FOREIGN KEY (iot_device_id) REFERENCES `iot_device` (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8C818A0989D9B62 ON iot_device (slug)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE climate_device_data DROP FOREIGN KEY FK_D1C0E17DA89D4A83');
        $this->addSql('ALTER TABLE light_device_data DROP FOREIGN KEY FK_D67CD287A89D4A83');
        $this->addSql('ALTER TABLE smart_energy_meter_device_data DROP FOREIGN KEY FK_92E0204BA89D4A83');
        $this->addSql('DROP TABLE climate_device_data');
        $this->addSql('DROP TABLE light_device_data');
        $this->addSql('DROP TABLE smart_energy_meter_device_data');
        $this->addSql('DROP INDEX UNIQ_8C818A0989D9B62 ON `iot_device`');
    }
}
