<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230516195730 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE device_data DROP FOREIGN KEY FK_24CA2659A89D4A83');
        $this->addSql('DROP TABLE device_data');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE device_data (id INT AUTO_INCREMENT NOT NULL, iot_device_id INT NOT NULL, data JSON NOT NULL, INDEX IDX_24CA2659A89D4A83 (iot_device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE device_data ADD CONSTRAINT FK_24CA2659A89D4A83 FOREIGN KEY (iot_device_id) REFERENCES iot_device (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
