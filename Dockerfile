FROM php:8.2-apache AS php

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf \
  \
  &&  apt-get update \
  &&  apt-get install -y --no-install-recommends \
  locales apt-utils git libicu-dev g++ libpng-dev libxml2-dev libzip-dev libonig-dev libxslt-dev unzip \
  \
  &&  echo "en_US.UTF-8 UTF-8" > /etc/locale.gen  \
  &&  echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
  &&  locale-gen \
  \
  &&  curl -sS https://getcomposer.org/installer | php -- \
  &&  mv composer.phar /usr/local/bin/composer \
  \
  && curl -sL https://deb.nodesource.com/setup_18.x | bash \
  && apt-get install nodejs \
  \ 
  &&  docker-php-ext-configure \
  intl \
  &&  docker-php-ext-install \
  pdo pdo_mysql opcache intl zip calendar dom mbstring gd xsl \
  \
  &&  pecl install apcu && docker-php-ext-enable apcu \
  && npm install --global yarn \
  && apt -y install make

WORKDIR /var/www/sym_react_webreathe_iot_monitoring
COPY composer.json package.json yarn.lock ./
RUN composer install && yarn install
COPY . ./
# RUN yarn build
RUN touch /root/initialized
COPY init-script.sh /
RUN chmod +x ./init-script.sh
# CMD ["./init-script.sh"]

FROM rabbitmq:3-management-alpine AS rabbitmq_3_alpine
WORKDIR /srv
COPY rabbitmq-script.sh ./
RUN chmod +x ./rabbitmq-script.sh
# CMD ["./rabbitmq-script.sh"]
EXPOSE 15675
