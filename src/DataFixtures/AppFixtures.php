<?php

namespace App\DataFixtures;

use App\Entity\ClimateDeviceData;
use App\Entity\DeviceCategory;
use App\Entity\DeviceData;
use App\Entity\IoTDevice;
use App\Entity\LightDeviceData;
use App\Entity\SmartEnergyMeterDeviceData;
use App\Entity\User;
use DateTime;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * Password's encoder
     *
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        // initialize a Faker\Generator instance
        $faker = Factory::create("fr_FR");
        $faker->seed(1337);

        $users      = [];
        $categories = [];
        $devices    = [];
        $datas      = [];
        // #####> Create the demo User #####
        $demoUser = new User();
        $hash = $this->passwordHasher->hashPassword($demoUser, "password"); // encode demo user password
        $demoUser->setEmail('demo@webreathe.com')
            ->setPassword($hash);

        $manager->persist($demoUser);
        $users[] = $demoUser;
        // #####< Create the demo User End #####
         
        // #####> Create fake Users #####
        for ($u = 0; $u < 4; $u++) {
            $user = new User();
            $hash = $this->passwordHasher->hashPassword($user, 'password');
            $user->setEmail($faker->email())
                ->setPassword($hash);

            $manager->persist($user);
            $users[] = $user;
        }
        // #####< Create fake Users End #####

        // #####> Create fake Device Category #####
        // Device Category Variables
        $deviceCategories = [
            // [
            //     "name"       => "LIGHT", 
            // ], 
            [
                "name"       => "SMART ENERGY METER", 
            ],
            [
                "name"       => "CLIMATE", 
            ],
        ];
        $dc = 0;

        // IoT Device Variables
        // $lightName     = ['Éclairage Salle de Séjour', 'Éclairage Extérieur', 'Éclairage Chambre', 'Éclairage Salle à Manger'];
        $applianceName = ['Prise TV Séjour', 'Prise Climatiseur Chambre'];
        $climatName    = ['Salle Séjour', 'Chambre'];
        $names         = [$applianceName, $climatName]; // $lightName, 
        
        // Device Data Variables
        $nbDay = 5;
        $currentYear = intval(date('Y'));
        $currentMonth = intval(date('m'));
        // $currentDay = intval(date('d'));

        $date_array = [];
        
        for ($j = 1; $j <= $nbDay; $j++) {
            for ($h = 0; $h < 24; $h++) {
                for ($m = 0; $m < 60; $m += 15) {
                    $date = new DateTime($currentYear . '-' . $currentMonth . '-' . $j . ' ' . $h . ':' . $m . ':00');
                    $date->format('Y-m-d H:i:s');

                    $date_array[] = $date;
                }
            }
        }

        foreach ($deviceCategories as $deviceCategory_) {
            $deviceCategory = new DeviceCategory();
            $deviceCategory->setName($deviceCategory_["name"]);

            // #####> Create fake IoT Device #####
            for ($d=0; $d < count($names[$dc]); $d++) { 
                $iotDevice = new IoTDevice();
                $iotDevice->setDeviceCategory($deviceCategory)
                          ->setDeviceId("" . $faker->unique()->randomNumber($nbDigits = 8, $strict = false))
                          ->setName($faker->unique()->randomElement($names[$dc]))
                          ->setCreatedAt(new DateTimeImmutable($faker->dateTimeBetween("-3 years", "now")->format("Y-m-d H:i:s")));
                          ;

                // #####> Create fake Device Data #####
                foreach ($date_array as $date_) {
                    $deviceData = $this->generateFakeDeviceData($faker, $iotDevice, $date_);
                    
                    $manager->persist($deviceData);
                    $datas[] = $deviceData;
                }
                // #####< Create fake Device Data End #####
                
                $manager->persist($iotDevice);
                $devices[] = $iotDevice;
            }
            // #####< Create fake IoT Device End #####
            $manager->persist($deviceCategory);
            $categories[] = $deviceCategory;
            $dc++;
        }
        // #####< Create fake Device Category End #####
        
        $manager->flush();
    }
    
    /**
     * IoT Device Data Generator function
     *
     * @param \Faker\Generator $faker
     * @param string $deviceCategoryName
     * @param IoTDevice $iotDevice
     * @return LightDeviceData | SmartEnergyMeterDeviceData | ClimateDeviceData
     */
    private function generateFakeDeviceData(\Faker\Generator $faker, IoTDevice $iotDevice, DateTime $date): LightDeviceData | SmartEnergyMeterDeviceData | ClimateDeviceData {
        
        $data = null;

        switch ($iotDevice->getDeviceCategory()->getName()) {
            case 'LIGHT':
                $data = new LightDeviceData();
                $data->setState($faker->boolean())
                     ->setDateTime($date)
                     ->setIotDevice($iotDevice);
                
                break;
            
            case 'SMART ENERGY METER':
                $activePower  = $faker->randomFloat(1, 0, 10);
                $activeEnergy = $activePower * $faker->randomFloat(1, 1.5, 3);
                $data         = new SmartEnergyMeterDeviceData();
                $data->setDateTime($date)
                     ->setVoltage($faker->randomFloat(1, 220, 230))
                     ->setActivePower($activePower)
                     ->setActiveEnergy($activeEnergy)
                     ->setIotDevice($iotDevice);
                
                break;
            
            case 'CLIMATE':
                $data = new ClimateDeviceData();
                $data->setDateTime($date)
                     ->setTemperature($faker->randomFloat(1, 24, 35))
                     ->setHumidity($faker->randomFloat(1, 60, 100))
                     ->setIotDevice($iotDevice);
                
                break;
            
            default:
                
                break;
        }

        return $data;
    }
}
