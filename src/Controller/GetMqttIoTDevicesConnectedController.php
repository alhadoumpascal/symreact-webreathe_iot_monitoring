<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GetMqttIoTDevicesConnectedController extends AbstractController
{

    public function __construct(
        private HttpClientInterface $client,
    ) {
    }

    public function __invoke()
    {
        $url = $this->getParameter('rabbitmq_connections_endpoint');
        $username = $this->getParameter('rabbitmq_mqtt_ws_username');
        $password = $this->getParameter('rabbitmq_mqtt_ws_password');
        // dump($username, $password, $url);
        // code execution continues immediately; it doesn't wait to receive the response
        $response = $this->client->request('GET', $url, [
            'auth_basic' => [$username, $password],
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        // trying to get the response content will block the execution until
        // the full response content is received
        $content = $response->toArray();
        
        return $this->json($content, 200);
        
    }
}
