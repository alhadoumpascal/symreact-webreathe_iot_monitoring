<?php

namespace App\Controller;

use App\Entity\IoTDevice;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class GetIotDeviceDataController extends AbstractController
{
    public function __construct(private EntityManagerInterface $manager)
    {
        
    }

    public function __invoke(IoTDevice $ioTDevice, Request $request) {
        // dump($request->get("startDate"));
        // dump($request->get("endDate"));
        $startDate = $request->get("startDate");
        $endDate = $request->get("endDate");

        $deviceCategory = $ioTDevice->getDeviceCategory()->getName();

        if($startDate && $endDate) {    
            $startDate = new DateTime($startDate . " 00:00:00");
            $endDate   = new DateTime($endDate . " 23:59:59");
            // dump($startDate);
            // dump($endDate);
            if($deviceCategory === "CLIMATE") {
                $climateData = $this->manager->createQuery("SELECT DISTINCT d.dateTime AS dt, d.temperature AS Temp, d.humidity AS Hum
                                            FROM App\Entity\ClimateDeviceData d
                                            JOIN d.iotDevice iot
                                            WHERE iot.id = :deviceId
                                            AND d.dateTime BETWEEN :startDate AND :endDate
                                            -- GROUP BY dt
                                            ORDER BY dt ASC")
                    ->setParameters(array(
                        "deviceId"   => $ioTDevice->getId(),
                        'startDate'  => $startDate->format('Y-m-d H:i:s'),
                        'endDate'    => $endDate->format('Y-m-d H:i:s'),
                    ))
                    ->getResult();

                // dump($climateData);
                $date = [];
                $temp = [];
                $hum  = [];

                foreach ($climateData as $d) {
                    $date[] = $d['dt']->format("Y-m-d H:i:s");
                    $temp[] = floatval(number_format((float) $d['Temp'], 1, '.', ''));
                    $hum[]  = floatval(number_format((float) $d['Hum'], 1, '.', ''));
                }

                return $this->json([
                    "date"     => $date,
                    "temperature"  => $temp,
                    "humidity"     => $hum
                ], 200);
            }
            else if($deviceCategory === "SMART ENERGY METER") {
                // dump("SMART ENERGY METER Data");
                $smartEnergyMeterData = $this->manager->createQuery("SELECT DISTINCT d.dateTime AS dt, d.activePower AS kW, d.activeEnergy AS kWh, d.voltage AS Volt
                                            FROM App\Entity\SmartEnergyMeterDeviceData d
                                            JOIN d.iotDevice iot
                                            WHERE iot.id = :deviceId
                                            AND d.dateTime BETWEEN :startDate AND :endDate
                                            -- GROUP BY dt
                                            ORDER BY dt ASC")
                    ->setParameters(array(
                        "deviceId"   => $ioTDevice->getId(),
                        'startDate'  => $startDate->format('Y-m-d H:i:s'),
                        'endDate'    => $endDate->format('Y-m-d H:i:s'),
                    ))
                    ->getResult();

                // dump($smartEnergyMeterData);
                $date         = [];
                $activePower  = [];
                $activeEnergy = [];
                $voltage      = [];

                foreach ($smartEnergyMeterData as $d) {
                    $date[]        = $d['dt']->format("Y-m-d H:i:s");
                    $activePower[] = floatval(number_format((float) $d['kW'], 1, '.', ''));
                    $activeEnergy[]  = floatval(number_format((float) $d['kWh'], 1, '.', ''));
                    $voltage[]  = floatval(number_format((float) $d['Volt'], 1, '.', ''));
                }

                return $this->json([
                    "date"         => $date,
                    "activePower"  => $activePower,
                    "activeEnergy" => $activeEnergy,
                    "voltage"      => $voltage
                ], 200);
            }
            else if($deviceCategory === "LIGHT") {
                dump("LIGHT Data");
            }
            
        }
        
        dd($ioTDevice->getDeviceCategory()->getName());
        
    }

}
