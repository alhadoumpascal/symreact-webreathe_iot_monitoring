<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\DeviceCategoryRepository;
use Cocur\Slugify\Slugify;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: DeviceCategoryRepository::class)]
#[ORM\HasLifecycleCallbacks()]
#[UniqueEntity("name", message: "A Device Category with this name already exists")]
#[ApiResource(
    order:  ["name" => "ASC"],
    operations: [
        new Get(
            normalizationContext: ["groups" => ["read:DeviceCategory:item"]],
            uriTemplate: "device-categories/{id}",
            requirements: ["id" => "\d+"]
        ),
        new GetCollection(
            normalizationContext: ["groups" => ["read:DeviceCategory:collection"]],
            uriTemplate: "device-categories"
        ),
        new Post(uriTemplate: "device-categories"),
        new Put(
            uriTemplate: 'device-categories/{id}',
            requirements: ['id' => '\d+'],
        ),
        new Patch(
            uriTemplate: 'device-categories/{id}',
            requirements: ['id' => '\d+'],
        ),
        new Delete(
            uriTemplate: 'device-categories/{id}',
            requirements: ['id' => '\d+'],
        )
    ]
)]
class DeviceCategory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["read:DeviceCategory:collection", "read:DeviceCategory:item", "read:IoTDevice:collection", "read:IoTDevice:item"])]
    private ?int $id = null;

    #[ORM\Column(length: 100, unique: true)]
    #[Groups(["read:DeviceCategory:collection", "read:DeviceCategory:item", "read:IoTDevice:collection", "read:IoTDevice:item"])]
    #[Assert\NotBlank(message: "Device Category name is required !")]
    #[Assert\Length(
        min: 3,
        minMessage: "Device Category name must be between 3 and 100 characters long !",
        max: 100,
        maxMessage: "Device Category name must be between 3 and 100 characters long !"
    )]
    #[Assert\Type(type: "string", message: "Value <<{{ value }}>> is not {{ type }} !")]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["read:DeviceCategory:collection", "read:DeviceCategory:item"])]
    #[Assert\Json()]
    private array $dataFormat = [];

    #[ORM\OneToMany(mappedBy: 'deviceCategory', targetEntity: IoTDevice::class)]
    #[Groups(["read:DeviceCategory:collection", "read:DeviceCategory:item"])]
    private Collection $ioTDevices;

    #[ORM\Column]
    #[Groups(["read:DeviceCategory:collection", "read:DeviceCategory:item"])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["read:DeviceCategory:collection", "read:DeviceCategory:item"])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(length: 150)]
    #[Groups(["read:DeviceCategory:collection", "read:DeviceCategory:item"])]
    private ?string $slug = null;

    public function __construct()
    {
        $this->ioTDevices = new ArrayCollection();
    }
    
    
    /**
     * Setting createdAt datetime when create Device Category
     * 
     * @return void
     */
    #[ORM\PrePersist]
    public function initializeCreatedAt()
    {
        if (empty($this->createdAt)) {
            $this->createdAt = new DateTimeImmutable(date('Y-m-d H:i:s'));
        }
    }

    /**
     * Setting updatedAt datetime when create or update Device Category
     * 
     * @return void
     */
    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function initializeUpdatedAt()
    {
        if (empty($this->updatedAt)) {
            $this->updatedAt = new DateTime(date('Y-m-d H:i:s'));
        }
    }

    /**
     * Setting slug for Device Category
     * 
     * @return void
     */
    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function initializeSlug()
    {
        $slugify = new Slugify();
        $this->slug = $slugify->slugify($this->name);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDataFormat(): array
    {
        return $this->dataFormat;
    }

    public function setDataFormat(?array $dataFormat): self
    {
        $this->dataFormat = $dataFormat;

        return $this;
    }

    /**
     * @return Collection<int, IoTDevice>
     */
    public function getIoTDevices(): Collection
    {
        return $this->ioTDevices;
    }

    public function addIoTDevice(IoTDevice $ioTDevice): self
    {
        if (!$this->ioTDevices->contains($ioTDevice)) {
            $this->ioTDevices->add($ioTDevice);
            $ioTDevice->setDeviceCategory($this);
        }

        return $this;
    }

    public function removeIoTDevice(IoTDevice $ioTDevice): self
    {
        if ($this->ioTDevices->removeElement($ioTDevice)) {
            // set the owning side to null (unless already changed)
            if ($ioTDevice->getDeviceCategory() === $this) {
                $ioTDevice->setDeviceCategory(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
