<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Repository\LightDeviceDataRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LightDeviceDataRepository::class)]
#[ApiResource(
    operations: [
        new Post(
            uriTemplate: 'light-devices/data',
        ),
    ],
)]
class LightDeviceData
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?bool $state = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateTime = null;

    #[ORM\ManyToOne(inversedBy: 'lightDeviceData')]
    #[ORM\JoinColumn(nullable: false)]
    private ?IoTDevice $iotDevice = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isState(): ?bool
    {
        return $this->state;
    }

    public function setState(bool $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getDateTime(): ?\DateTimeInterface
    {
        return $this->dateTime;
    }

    public function setDateTime(\DateTimeInterface $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getIotDevice(): ?IoTDevice
    {
        return $this->iotDevice;
    }

    public function setIotDevice(?IoTDevice $iotDevice): self
    {
        $this->iotDevice = $iotDevice;

        return $this;
    }
}
