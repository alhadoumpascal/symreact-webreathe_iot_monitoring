<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\UserRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ORM\HasLifecycleCallbacks()]
#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ["groups" => ["read:User:item"]]
        ),
        new GetCollection(
            normalizationContext: ["groups" => ["read:User:collection"]]
        ),
        new Post(),
        new Put(
            uriTemplate: "users/{slug}",
            requirements: ["slug" => "[a-z0-9]+(?:-[a-z0-9]+)*"]
        ),
        new Patch(
            uriTemplate: "users/{slug}",
            requirements: ["slug" => "[a-z0-9]+(?:-[a-z0-9]+)*"]
        ),
        new Delete(
            uriTemplate: "users/{slug}",
            requirements: ["slug" => "[a-z0-9]+(?:-[a-z0-9]+)*"]
        )
    ]
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["read:User:collection", "read:User:item"])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(["read:User:collection", "read:User:item"])]
    #[Assert\NotBlank(message: "The user's email is required !")]
    #[Assert\Email(message: "Value {{ value }} is not a valid email !")]
    #[Assert\Length(
        min: 3,
        minMessage: "The user's email must be between 3 and 180 characters long !",
        max: 180,
        maxMessage: "The user's email must be between 3 and 180 characters long !"
    )]
    private ?string $email = null;

    #[ORM\Column]
    #[Groups(["read:User:collection", "read:User:item"])]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    #[Assert\NotBlank(message: "The user's password is required !")]
    #[Assert\Length(min: 8, minMessage: "Password must be at least 8 characters long")]
    #[Assert\Type(type: "string", message: "Value <<{{ value }}>> is not {{ type }} !")]
    private ?string $password = null;

    #[ORM\Column]
    #[Groups(["read:User:collection", "read:User:item"])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["read:User:collection", "read:User:item"])]
    private ?\DateTimeInterface $updatedAt = null;

    /**
     * Setting createdAt datetime when create User
     * 
     * @return void
     */
    #[ORM\PrePersist]
    public function initializeCreatedAt()
    {
        if (empty($this->createdAt)) {
            $this->createdAt = new DateTimeImmutable(date('Y-m-d H:i:s'));
        }
    }

    /**
     * Setting updatedAt datetime when create or update User
     * 
     * @return void
     */
    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function initializeUpdatedAt()
    {
        if (empty($this->updatedAt)) {
            $this->updatedAt = new DateTime(date('Y-m-d H:i:s'));
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
