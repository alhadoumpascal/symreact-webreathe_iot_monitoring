<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Repository\SmartEnergyMeterDeviceDataRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SmartEnergyMeterDeviceDataRepository::class)]
#[ApiResource(
    operations: [
        new Post(
            uriTemplate: 'smart-energy-meter-devices/data',
        ),
    ],
    
)]
class SmartEnergyMeterDeviceData
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?float $voltage = null;

    #[ORM\Column(nullable: true)]
    private ?float $activePower = null;

    #[ORM\Column(nullable: true)]
    private ?float $activeEnergy = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateTime = null;

    #[ORM\ManyToOne(inversedBy: 'smartEnergyMeterDeviceData')]
    #[ORM\JoinColumn(nullable: false)]
    private ?IoTDevice $iotDevice = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVoltage(): ?float
    {
        return $this->voltage;
    }

    public function setVoltage(?float $voltage): self
    {
        $this->voltage = $voltage;

        return $this;
    }

    public function getActivePower(): ?float
    {
        return $this->activePower;
    }

    public function setActivePower(?float $activePower): self
    {
        $this->activePower = $activePower;

        return $this;
    }

    public function getActiveEnergy(): ?float
    {
        return $this->activeEnergy;
    }

    public function setActiveEnergy(?float $activeEnergy): self
    {
        $this->activeEnergy = $activeEnergy;

        return $this;
    }

    public function getDateTime(): ?\DateTimeInterface
    {
        return $this->dateTime;
    }

    public function setDateTime(\DateTimeInterface $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getIotDevice(): ?IoTDevice
    {
        return $this->iotDevice;
    }

    public function setIotDevice(?IoTDevice $iotDevice): self
    {
        $this->iotDevice = $iotDevice;

        return $this;
    }
}
