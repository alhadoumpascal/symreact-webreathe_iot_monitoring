<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\GetIotDeviceDataController;
use App\Controller\GetMqttIoTDevicesConnectedController;
use App\Repository\IoTDeviceRepository;
use Cocur\Slugify\Slugify;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: IoTDeviceRepository::class)]
#[ORM\Table(name: '`iot_device`')]
#[ORM\HasLifecycleCallbacks()]
#[UniqueEntity("deviceId", message: "An IoT Device with this ID already exists")]
#[ApiResource(
    // shortName: "iot-device",
    order: ["deviceCategory.name" => "DESC"],
    operations: [
        new Get(
            normalizationContext: ["groups" => ["read:IoTDevice:item"]],
            uriTemplate: 'iot-devices/{id}',
            requirements: ['id' => '\d+']
        ),
        new Get(
            normalizationContext: ["groups" => ["read:IoTDeviceData:collection"]],
            uriTemplate: 'iot-devices/{id}/data',
            requirements: ['id' => '\d+'],
            controller: GetIotDeviceDataController::class,
            // TODO: Change the swagger context summary for this endpoint
        ),
        new GetCollection(
            normalizationContext: ["groups" => ["read:IoTDevice:collection"]],
            uriTemplate: 'iot-devices',
        ),
        new GetCollection(
            normalizationContext: ["groups" => ["read:MqttIoTDevicesConnceted:collection"]],
            uriTemplate: 'iot-devices/connected',
            controller: GetMqttIoTDevicesConnectedController::class,
            // TODO: Change the swagger context summary for this end
        ),
        new Post(
            uriTemplate: 'iot-devices',
        ),
        new Put(
            uriTemplate: 'iot-devices/{id}',
            requirements: ['id' => '\d+'],
        ),
        new Patch(
            uriTemplate: 'iot-devices/{id}',
            requirements: ['id' => '\d+'],
        ),
        new Delete(
            uriTemplate: 'iot-devices/{id}',
            requirements: ['id' => '\d+'],
        )
    ],
    
)]
class IoTDevice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["read:IoTDevice:collection", "read:IoTDevice:item", "read:DeviceCategory:collection"])]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    #[Groups(["read:IoTDevice:collection", "read:IoTDevice:item", "read:DeviceCategory:collection"])]
    #[Assert\NotBlank(message: "IoT Device name is required !")]
    #[Assert\Length(
        min: 3,
        minMessage: "IoT Device name must be between 3 and 100 characters long !",
        max: 100,
        maxMessage: "IoT Device name must be between 3 and 100 characters long !"
    )]
    #[Assert\Type(type: "string", message: "Value <<{{ value }}>> is not {{ type }} !")]
    private ?string $name = null;

    #[ORM\Column(length: 100, unique: true)]
    #[Groups(["read:IoTDevice:collection", "read:IoTDevice:item", "read:DeviceCategory:collection"])]
    #[Assert\NotBlank(message: "IoT Device ID is required !")]
    #[Assert\Length(
        min: 3,
        minMessage: "IoT Device ID must be between 3 and 100 characters long !",
        max: 100,
        maxMessage: "IoT Device ID must be between 3 and 100 characters long !"
    )]
    #[Assert\Type(type: "string", message: "Value <<{{ value }}>> is not {{ type }} !")]
    private ?string $deviceId = null;

    #[ORM\ManyToOne(inversedBy: 'ioTDevices')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["read:IoTDevice:collection", "read:IoTDevice:item"])]
    #[Assert\NotBlank(message: "IoT Device Category is required")]
    private ?DeviceCategory $deviceCategory = null;

    #[ORM\Column]
    #[Groups(["read:IoTDevice:collection", "read:IoTDevice:item", "read:DeviceCategory:collection"])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(["read:IoTDevice:collection", "read:IoTDevice:item"])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(length: 150, unique: true)]
    #[Groups(["read:IoTDevice:collection", "read:IoTDevice:item"])]
    private ?string $slug = null;

    #[ORM\OneToMany(mappedBy: 'iotDevice', targetEntity: SmartEnergyMeterDeviceData::class, orphanRemoval: true)]
    private Collection $smartEnergyMeterDeviceData;

    #[ORM\OneToMany(mappedBy: 'iotDevice', targetEntity: ClimateDeviceData::class, orphanRemoval: true)]
    private Collection $climateDeviceData;

    #[ORM\OneToMany(mappedBy: 'iotDevice', targetEntity: LightDeviceData::class, orphanRemoval: true)]
    private Collection $lightDeviceData;

    public function __construct()
    {
        $this->smartEnergyMeterDeviceData = new ArrayCollection();
        $this->climateDeviceData = new ArrayCollection();
        $this->lightDeviceData = new ArrayCollection();
    }

    /**
     * Setting createdAt datetime when create IoT Device
     * 
     * @return void
     */
    #[ORM\PrePersist]
    public function initializeCreatedAt()
    {
        if (empty($this->createdAt)) {
            $this->createdAt = new DateTimeImmutable(date('Y-m-d H:i:s'));
        }
    }

    /**
     * Setting updatedAt datetime when create or update IoT Device
     * 
     * @return void
     */
    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function initializeUpdatedAt()
    {
        if (empty($this->updatedAt)) {
            $this->updatedAt = new DateTime(date('Y-m-d H:i:s'));
        }
    }

    /**
     * Setting slug for IoT Device
     *
     * @return void
     */
    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function initializeSlug()
    {
        $slugify = new Slugify();
        $this->slug = $slugify->slugify($this->name . '-' . $this->deviceId);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDeviceId(): ?string
    {
        return $this->deviceId;
    }

    public function setDeviceId(string $deviceId): self
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    public function getDeviceCategory(): ?DeviceCategory
    {
        return $this->deviceCategory;
    }

    public function setDeviceCategory(?DeviceCategory $deviceCategory): self
    {
        $this->deviceCategory = $deviceCategory;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, SmartMeterDeviceData>
     */
    public function getSmartEnergyMeterDeviceData(): Collection
    {
        return $this->smartEnergyMeterDeviceData;
    }

    public function addSmartMeterDeviceData(SmartEnergyMeterDeviceData $smartEnergyMeterDeviceData): self
    {
        if (!$this->smartEnergyMeterDeviceData->contains($smartEnergyMeterDeviceData)) {
            $this->smartEnergyMeterDeviceData->add($smartEnergyMeterDeviceData);
            $smartEnergyMeterDeviceData->setIotDevice($this);
        }

        return $this;
    }

    public function removeSmartMeterDeviceData(SmartEnergyMeterDeviceData $smartEnergyMeterDeviceData): self
    {
        if ($this->smartEnergyMeterDeviceData->removeElement($smartEnergyMeterDeviceData)) {
            // set the owning side to null (unless already changed)
            if ($smartEnergyMeterDeviceData->getIotDevice() === $this) {
                $smartEnergyMeterDeviceData->setIotDevice(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ClimateDeviceData>
     */
    public function getClimateDeviceData(): Collection
    {
        return $this->climateDeviceData;
    }

    public function addClimateDeviceData(ClimateDeviceData $climateDeviceData): self
    {
        if (!$this->climateDeviceData->contains($climateDeviceData)) {
            $this->climateDeviceData->add($climateDeviceData);
            $climateDeviceData->setIotDevice($this);
        }

        return $this;
    }

    public function removeClimateDeviceData(ClimateDeviceData $climateDeviceData): self
    {
        if ($this->climateDeviceData->removeElement($climateDeviceData)) {
            // set the owning side to null (unless already changed)
            if ($climateDeviceData->getIotDevice() === $this) {
                $climateDeviceData->setIotDevice(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, LightDeviceData>
     */
    public function getLightDeviceData(): Collection
    {
        return $this->lightDeviceData;
    }

    public function addLightDeviceData(LightDeviceData $lightDeviceData): self
    {
        if (!$this->lightDeviceData->contains($lightDeviceData)) {
            $this->lightDeviceData->add($lightDeviceData);
            $lightDeviceData->setIotDevice($this);
        }

        return $this;
    }

    public function removeLightDeviceData(LightDeviceData $lightDeviceData): self
    {
        if ($this->lightDeviceData->removeElement($lightDeviceData)) {
            // set the owning side to null (unless already changed)
            if ($lightDeviceData->getIotDevice() === $this) {
                $lightDeviceData->setIotDevice(null);
            }
        }

        return $this;
    }
}
