<?php

namespace App\Repository;

use App\Entity\SmartEnergyMeterDeviceData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SmartEnergyMeterDeviceData>
 *
 * @method SmartEnergyMeterDeviceData|null find($id, $lockMode = null, $lockVersion = null)
 * @method SmartEnergyMeterDeviceData|null findOneBy(array $criteria, array $orderBy = null)
 * @method SmartEnergyMeterDeviceData[]    findAll()
 * @method SmartEnergyMeterDeviceData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmartEnergyMeterDeviceDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SmartEnergyMeterDeviceData::class);
    }

    public function save(SmartEnergyMeterDeviceData $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SmartEnergyMeterDeviceData $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return SmartEnergyMeterDeviceData[] Returns an array of SmartEnergyMeterDeviceData objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SmartEnergyMeterDeviceData
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
