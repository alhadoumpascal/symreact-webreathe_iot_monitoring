<?php

namespace App\Repository;

use App\Entity\ClimateDeviceData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ClimateDeviceData>
 *
 * @method ClimateDeviceData|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClimateDeviceData|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClimateDeviceData[]    findAll()
 * @method ClimateDeviceData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClimateDeviceDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClimateDeviceData::class);
    }

    public function save(ClimateDeviceData $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ClimateDeviceData $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ClimateDeviceData[] Returns an array of ClimateDeviceData objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ClimateDeviceData
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
