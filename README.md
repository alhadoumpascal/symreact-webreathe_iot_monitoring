# SymReact-Webreathe_IoT_Monitoring


# Requirements

- Linux (debian or ubuntu) or MacOS X or Windows
- [docker](https://docs.docker.com/)
- [docker-compose](https://docs.docker.com/compose/)

# Technologies used

1. Backend
    1. Php:8.2.6/Symfony:6.2.10 with ApiPlatform:3.1
    2. MySQL:8.0.33
    3. Apache/2.4.56

2. Frontend
    1. ReactJS:18.2.0
    2. HTML/CSS
    3. Bootstrap:5.2.3
    4. Typescript:5.0.4

3. Others
    1. RabbitMQ 3.11.16
# Get project

```bash
git clone https://gitlab.com/alhadoumpascal/symreact-webreathe_iot_monitoring.git
 && cd symreact-webreathe_iot_monitoring
```
# Initialize project

**Note:** Before proceeding, please make sure that the following ports are open. If they are not, please release them:
* 8000, 8080
* 3000
* 3306
* 15672, 15675


1. Build/run containers with detached mode

    ```bash
    $ docker-compose up -d --build 
    ```
    Wait a few seconds and run the following command to see the running containers:
        
    ```bash
    $ docker-compose ps

                        Name                                   Command               State                                                              Ports
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    db_sym_react_webreathe_iot_monitoring           docker-entrypoint.sh mysqld      Up      3306/tcp, 33060/tcp
    phpmyadmin_sym_react_webreathe_iot_monitoring   /docker-entrypoint.sh apac ...   Up      0.0.0.0:3000->80/tcp
    rabbitmq_3                                      docker-entrypoint.sh rabbi ...   Up      15671/tcp, 0.0.0.0:15672->15672/tcp, 0.0.0.0:15675->15675/tcp, 15691/tcp, 15692/tcp, 25672/tcp, 4369/tcp, 5671/tcp, 5672/tcp
    www_sym_react_webreathe_iot_monitoring          docker-php-entrypoint apac ...   Up      0.0.0.0:8000->80/tcp, 0.0.0.0:8080->8080/tcp
    ```
**Note:** Please ensure that all containers are ****Up**** before proceeding.

2. Initialize RabbitMQ server container

    ```bash
    $ docker exec -it rabbitmq_3 bash ./rabbitmq-script.sh
    ```
3. Prepare Symfony app container

    ```bash
    $ docker exec -it www_sym_react_webreathe_iot_monitoring bash 
    $ make init-container
    $ exit
    ```
4. Restart containers to take consider the previous configurations 

    ```bash
    $ docker-compose stop
    $ docker-compose up -d
    ```

5. Enjoy :-)

## Usage

Just run `docker-compose up -d`, then:

* Symfony app: visit [http://localhost:8000](http://localhost:8000)  
* phpmyadmin: visit [http://localhost:3000](http://localhost:3000)

**Note:** Please use **root** username with empty password to access phpmyadmin. The application database name is **webreathe_iot_monitoring**


## How it works ?

Have a look at the `docker-compose.yml` file, here are the `docker-compose` built images:

* `db`: This is the MySQL database container,
* `www`: This is the PHP-FPM and apache webserver container in which the symfony application volume is mounted,
* `phpmyadmin`: This is the phpmyadmin container
* `rabbitmq`: This is a RabbitMQ message broker container that enables us to connect iot devices and user interfaces via the open, standardized protocols MQTT, MQTT over websockets, in order to visualize the measurements made by the devices in real time.

This results in the following running containers:

```bash
$ docker-compose ps
           Name                                  Command                  State              Ports            
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
db_sym_react_webreathe_iot_monitoring            "docker-entrypoint.s…"     Up      3306/tcp, 33060/tcp      
phpmyadmin_sym_react_webreathe_iot_monitoring    "/docker-entrypoint.…"     Up      0.0.0.0:3000->80/tcp          
rabbitmq_3                                       "docker-entrypoint.s…"     Up      4369/tcp, 5671-5672/tcp, 0.0.0.0:15672->15672/tcp, 15671/tcp, 15691-15692/tcp, 25672/tcp, 0.0.0.0:15675->15675/tcp
www_sym_react_webreathe_iot_monitoring           "docker-php-entrypoi…"     Up      0.0.0.0:8080->8080/tcp, 0.0.0.0:8000->80/tcp      
```
